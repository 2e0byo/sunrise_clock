#+Title: Sunrised Clock/Lamp Manual
* Introduction
* Functionality
* Interface Paradigm
On top of the lamp is the bulb and shade.  In front of the lamp, on
the top, is the /Top Button/ or /Enter Button/.  On the front of the
lamp is the screen.  Below the scree are the /Down Button/ and /Up
Button/.

What is /displayed/ on the screen is controlled with the /Down Button/
and /Up Button/.  What the Lamp/Clock /does/ is controlled with the
/Enter Button/ (aka /Top Button/).  Thus the first thing the Clock
will ask you to do is to set the time.  To do this, use the up and
down buttons until the correct value is reached, and then press the
enter button to advance to the next digit.

All buttons can be /pressed/ or /held/.  /Holding/ the up and down
buttons generally causes their input to happen rapidly until they are
released (an exception is for setting the brightness of the lamp in
lamp mode), or to happen once and wait, where that makes more sense.
Holding the enter button generally causes the display to reset (an
exception is when setting a snooze after the alarm has elapsed).

The display is menu driven.  It boots up in the Main Menu.  Pressing
the up and down keys allows one to scroll through the menu.  Some
items on the Main Menu are SubMenus: pressing the enter button causes
one to enter them.  Holding down the enter button returns you to the
main menu.  /Pressing/ the enter button ‘enters’ whatever is displayed
on the menu (except in clock mode, where it turns the lamp on or
off).  When in a /menu/ the device will time out, first returing to
the main menu after about 40 seconds of inactivity, and then turning
the backlight off.  When in some function---when one has pressed
‘enter’ on something which isn’t just a submenu, for instance ‘Set
Alarm Time’---the display will not time out.

* Operation

Plug the Lamp/Clock in and turn it on.  Set the time as requested.
The display will now be in Clock Mode: this is the default position to
which it will respond.  If you leave it for aroud forty seconds, the
backlight will turn off.  Press either the up or down button and the
display will come on again (without doing anything else).

** Clock

*** Setting the time

Go to System Setup -> Set Time and set the time.  As with all menu
entries, hold down the enter button once you have returned to the
‘System Setup’ menu, or wait forty seconds to return to clock mode.

*** Rate

All clocks have a rate---the amount they lose or gain in a day.  The
clock in this lamp is implemented in software based on the 8MHz
crystal (internally multiplied to 48Mhz).  This crystal is very
stable, but not very precisely calibrated to 8MHz.  Thus one might
think one could build a clock with something like this:

#+BEGIN_SRC 
while (true) {
   print_time();
   ++time;
   if (time > day) time = 0;
   sleep(1s - time_to_do_everything_else);
}
#+END_SRC

And this is indeed what this clock does---the sleep is done with an
interrupt timer and thus the ~time_to_do_everything_else~ doesn’t
really matter.  But sleeping ‘1s’ would result in quite an innaccurate
clock.  We thus sleep for a value called ‘tickPeriod’ which is about
997ms (nominal).  This value can be set from System Setup -> Set Tick
Period.  If the clock is running fast, /increase/ it.  If it is
running slowly, /decrease/ it.

** Alarm

The device has two alarm modes: firstly, the lamp fades on for a
defined period; secondly, if enabled, it beeps like a standard alarm
clock, to make sure you get up.

Every time the clock counter is advanced the clock checks whether the
time is equal to the alarm time - the fade time (obviously, this value
is precomputed).  /Thus if you try to set an alarm for less than Fade
Time minutes in the future it won’t trigger until tomorrow./

*** Set Alarm

Press the Up button from the main screen, or go to Setup Alarm -> Set
Alarm Time.  Alarms are preserved over power cycles.

*** Enable/Disable Alarm

Go to Setup Alarm -> Enable/Disable.

*** Enable/Disable Ring

Set the Alarm Time.  The current alarm will be presented by default.
After the alarm time is set, the clock will ask if you want ring
enabled or not.

*** Set Fade On Time

Go to System Setup -> Set Fade Time

Longer fade times are more like ‘natural’ sunrise.

*** Turn off a sounding alarm

/Press/ the top button.

*** Snooze an Alarm

/Hold/ the top button.  Snoozes between 3 minutes and 59 minutes can
be set.  The Fade On Time will be set appropriately.  When a snooze is
set, the main display shows the time remaining until the snooze
elapses.  /Pressing/ the top button when a snooze is enabled allows
you to edit the snooze.  Currently the only way to cancel a snooze is
to set it to 3 minutes, which causes the alarm to begin after a
minute.  Once an alarm is elapsing it can be cancelled in the usual
fashion.

This is not too elegant, but neither is snoozing ;)  More seriously,
there are only three buttons, and I couldn’t think of any nice way of
cancelling a snooze.


** Sunset

As well as acting as a sunrise, the lamp can act as a sunset to help
you fall asleep.  Press the down button from the main screen to
navigate to ‘start sunset’.  Set the sunset time and press enter.

Sunset can be cancelled by pressing the top button.

** Lamp

The device, obviously, features a lamp.  This lamp is used for the
Sunrise/sunset functions, but can also be used independently.

*** Turning the Lamp On/Off

From the main screen (to which the device defaults when there is no
activity) press the top buton.  The lamp will turn on or off.

*** Adjusting the Lamp Brightness

/Hold/ the down or up button from the main screen.  Set the
brightness, then press the top/enter button to return to the main screen.

*** Max/Min Brightness

Due to the way filament lamps respond to being fed rectified DC the
maximum brightness available with this lamp is significantly above the
brightness observed if the bulb is put in an ordinary light fitting.
(Specifically: the bulb’s reactance ‘smooths’ the troughs between
peaks so that Vmean >> Vrms.)  This would cause the bulb to fail
quickly, and makes the light harsh.  For this reason the maximum
brightness has been set so that Vmean = 240v, or so that no difference
should be observable if the bulb is plugged into an ordinary light
fitting.  This maximum can be changed, although it should not be
increased by much if you want the bulb to have a reasonable life.  It
might, however, be necessary to decrease it.

Likewise, the bulb does not turn on at all until a certain threshold
is reached.  For this reason we also set the minimum brightness, at
the point where the bulb is just glowing.  Bear in mind that if the
bulb has been on for even a short while it will be warm and thus turn
on easier, so you should err on the side of ‘glowing’ not ‘just’.
This value will change slightly as the bulb ages and may change
significantly between bulbs, particularly if a different type (for
instance a halogen bulb) is used.

These parameters can be set from the main menu by going to System
Setup -> Set Min Brightness and System Setup -> Set Max Brightness.

** Reset

Go to Setup Menu -> Factory Reset.

* Oddities
Due to the way the clock is implemented (i.e. on a small
microcontroller, by an amateur) there are a few oddities.  A faster
microcontroller with a proper Real Time Operating System, or possibly
just better code written from scratch by a better programmer would
probably avoid these.  Suggestions for mitigation are welcome!

- Clock flicker on turning lamp On/Off :: The lamp is turned on/off
     using an interrupt routine.  Enabling the interrupt takes a few
     cycles as does servicing the interrupt during the fade.
     Furthermore, the clock screen is just another menu entry, and the
     whole menu loop is called (very quickly) when the button is
     pressed.  I have not noticed this effect much since tuning
     timings.  Rest assured that it has nothing to do with the
     clock itself, which is incremented in the background (using
     another interrupt routine), but only the display.

- Screen displaying garbage :: This can happen if electrical noise is
     picked up on the lcd terminals (unlikely), if the microprocessor
     misbehaves (for instance due to excess heat), or if noise is
     coupled through the power supply (most likely).  It should not
     happen, but if it does happen, turn the lamp off and on again.
     (Note: previous iterations of this code corrupted the screen due
     to poor interrupt code.  This has been resolved.)

- Short fades not precisely timed :: Fades on/off of over
     (maxBrightness - minBrightness) seconds use the system clock.
     Fades under this number (generally <~700s / <~12 mins) use an
     interrupt timer of tickPeriod/100.  This is integer division and
     there will likely be some remainder; as a consequence the actual
     total fade time will not be precisely the number of seconds the
     system clock has counted.  Unless you time it you should not
     notice the difference.  Short fades are only used by default when
     snoozing.

* Safety

There are no particular safety hazards with this device /if used
properly/.  However, there are some issues you should be aware of:

** DC

The lamp on top of the base is run of DC (direct current) not AC
(alternating current).  The socket in the top of the lamp has a
maximum of about 330v DC accross its terminals.  High voltage DC is
inherently dangerous: it will arc easily, it ‘grabs’ (causes muscles
to contract and hold a live contact, unlike AC which ‘throws’ due to
the rapidly changig polarity) making serious injury or death a
possibility.

Fortunately there is no way the user can come in contact with this DC
voltage in general usage.  However, when the lamp is replaced care
must be taken NOT TO TOUCH THE CONTACTS: this is always true when
replacing a bulb, (and is very hard to do: one has to remove the bulb
and then jab one’s fingers in) but is doubly true here.  Likewise, the
lamp should NEVER be left without a bulb (even a blown bulb) in the
socket.  If you are nervous about this, unplug the lamp, wait five
minutes and then change the bulb.

There is no danger from the metal lamp fitting.  It is properly
earthed.  It, and the lamp shade, and the metal side of the bulb for
that matter, can be touched without danger of electrocution, although
they may be hot.

Should any internal components fail they /should/ fail safe.  But even
if they do not, an internal fire is inherently safe as there is little
oxygen and almost nothing to burn.

** Fire

The shade on this lamp is made from paper and has not been fire
tested, unlike commercial lampshades.  It has been run for about six
hours at full brightness (much higher than the maximum brightness the
lamp is set up to allow by default).  Although the lamp fitting became
very hot, the lampshade was barely warm.  The insulation between the
lamp fitting and the lamp base protects the latter from the heat.  The
undersides of the mounting bolts are similarly insulated with
thermally non-conductive washers to prevent heat transfer into the
plastic case.

* Maintenance

** Cleaning

Just wipe with a damp cloth.

** Replace Fuse

Firstly: why did the fuse blow?  If it happened in ordinary use, take
care when plugging the lamp in again.  If it happened when the bulb
was changed, check the bulb in an ordinary lamp fitting.  If it
happens continually, decrease the bulb wattage.

A 3A fuse should be used.  Lower than that is liable to blow due to
the poor power factor of the dimmer.  Higher is unsafe.

** Replacing Bulb

When the bulb blows it should be replaced with a filament bulb.
Either a ‘classic’ incandescent bulb or a halogen bulb (or any other
kind of filament bulb you can find) can be used.  LED bulbs, even
those advertised as ‘dimmable’, are not suitable and will likely be
destroyed.  CFLs are definitely not suitable.

If the supply of filament bulbs dries up entirely I will replace the
dimming module with something capable of handling ‘dimmmable’ led
bulbs.

** Reprogramming

The lamp/clock is capable of being easily reprogrammed in the field
via the USB B socket on the back the base.  For this purpose a python
script (~uploader.py~) is required (or some other implementation which
can talk to a pinguino bootloader).

When you have obtained the new firmware, use a pin to press the button
behind the small hole beside the usb socket.  The display will turn
off, the lamp will turn on fully and the device will be ready for new
firmware.  Now run the uploader script from the computer.  The lamp
will start running immediately the firmware is uploaded.

N.B. do not run the lamp for long periods with the controller in
firmware update mode, as the bulb is not being controlled and could
blow.

** Source Code

The source code for this lamp (together with this manual) is available
at

https://gitlab.com/2e0byo/sunrise_clock

It was initially written using the [[http:pinguino.cc][Pinguino]] ide, and then the
generated C code was rather crudely copied into a repository and
edited by hand.  Currently it will not compile without the [[https://github.com/PinguinoIDE/pinguino-libraries][Pinguino
Libraries]], although several of these have been modified.  The compiled
code is designed to be uploaded to a PIC18f device flashed with the
pinguino bootloader, although it will probably work if programmed
directly with a programmer.

* Feedback

A lot of thought has gone into the interface design---hopefully the
most used functions are most easily accessible, and things increment
at sensible rates.  Things roll over when it seems sensible, and cap
at max/min when it doesn’t.  Feedback on all these decisions and
others is welcome.

* Technical

This clock is implemented using a pic18f2550 microcontroller.  It
would be /much/ easier to use a pic with inbuilt RTCC (Real Time Clock
and Calendar), or an external RTC(C) module.  Likewise, a more modern
pic with multiple PWM timers would make the audio output more
straightforward, and a faster pic would enable playing audio from an
SDcard (I think this would be just about possible at the moment, but
the overhead would be too high to have risked implementing it).  But
the challenge was to use what I had, not what would be ideal.  So the
‘real time clock’ is implemented in software, as a 24-bit unsigned
integer incremented by an interrupt timer every tickPeriod
milliseconds.  This is relatively stable if not perfectly accurate,
but probably good enough for a mains-powered clock which is liable to
be turned off by mistake quite frequently anyhow.

The lamp fade circuitry is a compromise: a ‘proper’ trailing-edge
dimmer would be require sofware control, and I didn’t want the hassle
of writing it.  Another pic could be dedicated to it, but I only have
40-pin devices left---a bit of a waste.  Using DC is easier than
PWMing AC, which I did try, but the power supply for the mosfet gate
becomes a problem.  The only ‘mains-rated’ components I had were from
old computer PSUs, which pretty much dictated the architecture.  The
bridge rectifier, mosfet, capacitors and optoisolator are all from the
same PSU.  The other parts for the lamp were already in the junkbox,
including the lcd, which is woefully slow to update, but there we go.
The cabling is from an old IDE cable.

The speaker is driven via a low-pass filter on the offchance I get
round to implementing ‘proper’ audio playback later.  There is,
however, no sd card slot at present as I didn’t have one to hand.

The controller is powered from an SMPSU robbed from an old ‘wall-wart’
I had in the cupboard.
