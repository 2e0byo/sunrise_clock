#define ANALOGWRITE
#define INTDISABLE 
#define LCDCLEAR
#define LCDCURSOR
#define LCDHOME
#define LCDNOCURSOR
#define LCDPRINT
#define LCDPRINTF
#define LCDSETCURSOR
#define LINEOUT PWM2
#define TMR0INT
#define TMR1INT
#define INT0INT
#define backlight 9
#define upButton 22
#define downButton 23
#define lamp 12
#define relay 24
#define longPress 5
#define enterButton 21
#define mainsFreqPin 0
#include "analog.c"
#include <audio.c>
#include "eeprom.c"
#include <delayms.c>
#include <digitalp.c>
#include <digitalr.c>
#include <digitalw.c>
#include <interrupt.c>
#include "lcdlib.c"
#include <macro.h>
#include <typedef.h>
