/* Declarations */
void writeEepromFlags ();
u8 pollButton(u8 button, char waitLongPress, char wait);
void debounceButton(u8 button);
void readEepromFlags ();
u8 ring();
void tick();
void printAlarmState();
void timeToHMS(__uint24 t, u8 *h, u8 *m, u8 *s);
void writeEepromFlags ();
void homeScreen();
void printSnooze();
void editSnooze();
void enterMainMenu();
void printTime();
typedef void (* voidFnPointer) ();

/* vars */
char firmwareVersion[] = "v1.4.1";

volatile char clockReady;
char fadeReady;
bit stopClock = 0;
bit stopRing = 0;
u8 clockInterrupt;
bit lampState = 0;
u8 menuTimeout = 0;
u8 index = 0;
bit menuChange ;
bit lampInUse;
u8 pps = 50;

unsigned short maxBrightness;
unsigned short minBrightness;
unsigned short brightness = 0;

#define day 86399

volatile __uint24 time = 0;
__uint24 fadeTime;
__uint24 alarmTime;	
volatile __uint24 realAlarmTime;
__uint24 savedAlarmTime;
u8 alarmH, alarmM, alarmS;			/* for printing */
u8 savedFadeOnMinutes;
u8 fadeOnMinutes;
u8 snoozeMinutes = 3;

bit alarmEnabled;
bit alarmFlag;
bit ringOn;

u8 eepromMinBrightness = 0;
u8 eepromMaxBrightness = 2;
u8 eepromFadeOnMinutes = 4;
u8 eepromAlarmTime = 7;		/* 24-bit = 3 bytes */
u8 eepromFlags = 10;

typedef struct
{
    int freq;
    int rest;
} Note;


Note melody[] = {
    {NOTE_G6, 4},
    {NOTE_G6, 4}};

void writeEepromFlags () {
  u8 flags;
  flags = flags | alarmEnabled;
  flags = (flags << 1) | ringOn;
  EEPROM_write8(eepromFlags, flags);
}


typedef struct
{
  char name[16];
  voidFnPointer enterFunc;
  voidFnPointer displayFunc;		/* function to run on switching to screen */
  voidFnPointer timeoutFunc;		/* Timeout */
  voidFnPointer exitCurrentMenu;	/* Long Press */
  voidFnPointer longDownFunction;	/* Hold Down Button */
  voidFnPointer longUpFunction;		/* Hold Up Button */
} menu;

menu *currentMenu = NULL;
volatile unsigned short idleCount = 0;
volatile u8 menuSize;

u8 pollButton(u8 button, char waitLongPress, char wait) {
  while (wait && digitalread(button)) {
    Delayms(100);
  }
  u8 count = 0;
  if (!digitalread(button)) {
    while (!digitalread(button)) {
      count ++;
      if (waitLongPress && count > longPress) {
	return 2;
      }
      Delayms(100);
    }
    Delayms(100);		/* debounce */
  return 1;
  }
  return 0;
}

void debounceButton(u8 button) {
  while (!digitalread(button)) {
    Delayms(50);
  }
  Delayms(100);			/* debounce */
}


void readEepromFlags () {
  u8 flags = EEPROM_read8(eepromFlags);
  ringOn = flags & 1;
  alarmEnabled = (flags >> 1) & 1;
}

/* Hardware functions */
u8 ring() {
  if (!ringOn){
    u8 ret = pollButton(enterButton, 1, 1);
    return ret;
  }
  stopRing = 0;
  
  while (!stopRing) {
    u8 pauseBetweenNotes;
    u8 noteDuration;

    //    Audio_init(TAPEQUALITY);
    Audio_staccato();
    
    for (u8 thisNote = 0; thisNote < 2; thisNote++) {
      noteDuration = 250 / melody[thisNote].rest;
      Audio_tone(LINEOUT, melody[thisNote].freq, noteDuration);
      pauseBetweenNotes = noteDuration +  noteDuration / 30;
      Delayms(pauseBetweenNotes);
      
      if (clockReady) printTime();

      if (!digitalread(enterButton)) {
	stopRing = 1;
	break;
      }
    }
    
    
    Audio_noTone(LINEOUT);
    /* pause between notes, reusing variable */
    for (u8 thisNote = 0; thisNote < 100; thisNote++) {
      Delayms(10);
      if (!digitalread(enterButton)) {
	stopRing = 1;
	break;
      }
    }

  }
  u8 ret = pollButton(enterButton, 1, 1);
  return ret;
}

void resetDisplay() {
  lcd_begin(16,2);
  lcd_clear();
}

void tick(){
  --pps;
  if (pps) {
    return;			/* pps > 0 */
  } else {
    pps = 50;
  }
  
  if (stopClock) {
    return;
  }
    
    time++;
    if (time > day) {
      time = 0;
    }
    clockReady = 1;

    if (realAlarmTime == time) {
      alarmFlag = 1;
    }
}

void printAlarmState(){
  if (savedAlarmTime) {
    lcd_print("Snoozing...");
    return;
  }
  if (alarmEnabled) {
    lcd_print("Alarm On: ");
  }
  else {
    lcd_print("Alarm Off:");
  }
  lcd_printf(" %02u:%02u", alarmH, alarmM);
}


void timeToHMS(__uint24 t, u8 *h, u8 *m, u8 *s) {
  uldiv_t result;
  result = uldiv(t, 3600);
  *h = (u8)result.quot;
  result = uldiv(result.rem, 60);
  *m = (u8)result.quot;
  *s = (u8)result.rem;
}

void printTime() {
    lcd_setCursor(4, 1);
    u8 h, m, s;
    timeToHMS(time, &h, &m, &s);
    lcd_printf("%02u:%02u:%02u", h, m, s);
    clockReady = 0;
}

void printSnooze() {		/* just prints time to alarm */
  lcd_setCursor(11,0);
  u8 h, m, s;
  timeToHMS(alarmTime - time, &h, &m, &s);
  lcd_printf("%02u:%02u", m, s);
}

void printAlarmStateLowerLine() {
  lcd_setCursor(0,1);
  if (alarmEnabled) {
    lcd_print("(On: ");
  }
  else {
    lcd_print("(Off:");
  }
  lcd_printf(" %02u:%02u)", alarmH, alarmM);
}


void fadeISR() {
  fadeReady = 1;
}

u8 fade(__uint24 t, char direction) {
  unsigned short steps = (analogread(lamp) - minBrightness);
  uldiv_t result;

  /* can we use system clock?  More accurate if so... */
  char *readyFlag = NULL;
  u8 fadeInterrupt = 0;
  if (t >= steps) {
    result = uldiv(t, steps);
    readyFlag = &clockReady;
  }
  else {
    result = uldiv(t*100, steps);
    fadeInterrupt = OnTimer1(fadeISR, INT_MILLISEC, 9);
    readyFlag = &fadeReady;
  }
  u8 endFlag = 0;
  for (;;) {
    if (direction) {
      if (brightness == maxBrightness) break;
      ++brightness;
    }
    else {
      if (brightness == minBrightness) break;
      --brightness;
    }
    analogwrite(lamp, brightness);
    unsigned short pause = 0;
      
    while (pause < result.quot) {
      while (!*readyFlag) {
	Delayms(5);
	endFlag = pollButton(enterButton, 1, 0);
	if (endFlag) {
	  if (fadeInterrupt) IntDisable(fadeInterrupt);
	  return endFlag;
	}
      }
      if (clockReady) printTime();
      *readyFlag = 0;
      pause++;
    }

    if (result.rem > 0) {
      while (!*readyFlag) {
	Delayms(5);
	endFlag = pollButton(enterButton, 1, 0);
	if (endFlag) {
	  if (fadeInterrupt) IntDisable(fadeInterrupt);
	  return endFlag;
	}
      }
      if (clockReady) printTime();
      *readyFlag = 0;
      result.rem--;
    } 
  }
  if (fadeInterrupt) IntDisable(fadeInterrupt);
  return 0;
}

u8 fadeOn() {
  u8 ret;
  brightness = minBrightness;
  digitalwrite(lamp, 0);	/* make sure defined */
  digitalwrite(relay, 1);
  resetDisplay();
  lcd_print("Ring! Ring! Ring!");
  ret = fade(fadeOnMinutes * 60, 1);
  return ret;
}

void backlightOff(){
  digitalwrite(backlight, LOW);
}

char toggleValue(char value, char yes[16], char no[16]){
  lcd_clear();
  lcd_setCursor(0,1);
  if (value) {
    lcd_print(yes);
  }
  else {
    lcd_print(no);
  }
  lcd_setCursor(0,1);
  
  while (digitalread(enterButton)){
    if (!digitalread(upButton) || !digitalread(downButton)){
      if (value){
	value = 0;
	lcd_print(yes);
      }
      else {
	value = 1;
	lcd_print(no);
      }
      lcd_setCursor(0, 1);
      while (!digitalread(upButton) || !digitalread(downButton)){
	Delayms(100); 		
      }
    }
    Delayms(100);
  }
  debounceButton(enterButton);
  return value;
}

void toggleAlarmEnabled () {
  alarmEnabled = toggleValue(alarmEnabled, "Alarm Enabled ", "Alarm Disabled");
  writeEepromFlags();
}

unsigned short setValue(unsigned short value, unsigned short min, unsigned short max, u8 col) {
  u8 ret, ret2;
  while (digitalread(enterButton)) {
    if (!ret && !ret2) {
      ret = pollButton(upButton, 1, 0);
    }
    if (ret == 1) {
      if (value < max) {
	value += 1;
      } else {
	value = min;
      }
    }
    else if (ret == 2) {
      if (value <= max - 2) {
	value +=2;
      } else {
	value = min;
      }
    }

    if (!ret && !ret2) {
      ret2 = pollButton(downButton, 1, 0);
    }
    if (ret2 == 1) {
      if (value > min) {
	value -= 1;
      } else {
	value = max;
      }
    }
    else if (ret2 == 2) {
      if (value >= min + 2) {
	value -= 2;
      }
      else {
	value = max;
      }
    }

    if (ret || ret2) {
      if (max > 999) {
	lcd_printf("%04u", value);
      }
      else if (max > 99) {
	lcd_printf("%03u", value);
      } else {
	lcd_printf("%02u", value);
      }
      lcd_setCursor(col, 1);
    }
    
    if (digitalread(downButton) && digitalread(upButton)) {
      ret = 0;
      ret2 = 0;
    }
    Delayms(100);
  }
  debounceButton(enterButton);
  return value;
}

void setTime(__uint24 *t, char name[16]){
  
  lcd_clear();
  lcd_print(name);
  lcd_setCursor(4,1);
  u8 h, m, s;
  timeToHMS(*t, &h, &m, &s);
  
  lcd_printf("%02u:%02u:%02u", h, m, s);
  lcd_setCursor(4,1);
  lcd_cursor();
  h = setValue(h, 0, 23, 4);
  lcd_setCursor(7, 1);
  m = setValue(m, 0, 59, 7);
  lcd_setCursor(10, 1);
  s = setValue(s, 0, 59, 10);
  lcd_noCursor();
  *t = s + ((__uint24)m * 60) + ((__uint24)h * 3600);
}

void setClock() {
  lcd_clear();
  stopClock = 1;
  setTime(&time, "Time:");
  stopClock = 0;
}


void adjustAlarm() {
  realAlarmTime = alarmTime - fadeOnMinutes * 60;
  if (realAlarmTime > day) {	/* overflowed */
    realAlarmTime = day - fadeOnMinutes * 60 + alarmTime;
  }
  timeToHMS(alarmTime, &alarmH, &alarmM, &alarmS);
}


void setAlarm() {
  setTime(&alarmTime,"Alarm:");
  EEPROM_write24(eepromAlarmTime, alarmTime);
  adjustAlarm();
  ringOn = toggleValue(ringOn, "Ring Enabled", "Ring Disabled");
  writeEepromFlags();
}

void setFadeOnMinutes() {
  lcd_clear();
  lcd_print("Fade on time:");
  lcd_setCursor(0,1);
  lcd_printf("%02u",fadeOnMinutes);
  lcd_print(" Minutes");
  lcd_setCursor(0,1);
  fadeOnMinutes = setValue(fadeOnMinutes, 0, 59, 0);
  EEPROM_write8(eepromFadeOnMinutes, fadeOnMinutes);
}

u8 lampInterrupt;
void lampOnISR() {
  lampInUse = 1;
  if (++brightness < maxBrightness) {
    analogwrite(lamp, brightness);
  }
  else {
    IntDisable(lampInterrupt);
    lampInUse = 0;
  }
}


void lampOffISR() {
  lampInUse = 1;
  if (brightness && --brightness  > 0 ) {
    analogwrite(lamp, brightness);
  }
  else {
    digitalwrite(relay, 0);
    IntDisable(lampInterrupt);
    lampInUse = 0;
  }
}

void lampOn() {
  if (lampInUse || lampState) {
    return;
  }
  digitalwrite(lamp, 0);	/* make sure defined */
  digitalwrite(relay, 1);
  resetDisplay();
  if (brightness == 0) brightness = minBrightness;
  lampInterrupt = OnTimer1(lampOnISR, INT_MILLISEC, 2);
  lampState = 1;
}


void lampOff() {
  if (lampInUse) {
    return;
  }
  lampInterrupt = OnTimer1(lampOffISR, INT_MILLISEC, 2);
  lampState = 0;
}

void toggleLamp () {
  menuChange = 0;
  if (!lampState) {
    lampOn();
  }
  else {
    lampOff();
  }
}



void setBrightness(unsigned short *br, char instruction[16], char name[12], unsigned short max, unsigned short min) {
  lcd_clear();
  lcd_print(instruction);
  lcd_setCursor(0,1);
  analogwrite(lamp,  *br);
  lcd_printf("%s%04u", name, *br);
  lcd_setCursor(0,1);
  debounceButton(upButton);
  debounceButton(downButton);
  u8 ret, ret2;
  while (digitalread(enterButton)) {
    if (!ret && !ret2) {
      ret = pollButton(upButton, 1, 0);
    }
    if (ret == 1) {
      if (*br < max) {
	*br += 1;
      }
    }
    else if (ret == 2) {
      if (*br < max - 8) {
	*br += 8;
      }
      else {
	*br = max;
      }
    }

    if (!ret && !ret2) {
      ret2 = pollButton(downButton, 1, 0);
    }
    if (ret2 == 1) {
      if (*br > min) {
	*br -= 1;
      }
    }
    else if (ret2 == 2) {
      if (*br > min + 8) {
	*br -= 8;
      }
      else {
	*br = 0;
      }
    }

    analogwrite(lamp,  *br);
    lcd_printf("%s%04u", name, *br);
    lcd_setCursor(0,1);
    if (digitalread(downButton) && digitalread(upButton)) {
      ret = 0;
      ret2 = 0;
    }
    Delayms(50);
    
  }

  debounceButton(enterButton);

  lampInUse = 0;
}


void adjustLamp () {
  if (!lampState) {
    lampOn();
  }
  setBrightness(&brightness, "Set Level", "Lamp: ", maxBrightness, minBrightness);
}

void setMinBrightness() {
  digitalwrite(relay, 1);
  setBrightness(&minBrightness, "Set to glowing", "Min Bright: ", 1023, 0);
  brightness = 0;
  digitalwrite(lamp, 0);
  digitalwrite(relay, 0);
  //lampOff();
  EEPROM_write16(eepromMinBrightness, minBrightness);
}

void setMaxBrightness() {
  digitalwrite(relay, 1);
  setBrightness(&maxBrightness, "Set to full on", "Max Bright: ", 1023, 0);
  //  lampOff();
  brightness = 0;
  digitalwrite(lamp, 0);
  digitalwrite(relay, 0);
  EEPROM_write16(eepromMaxBrightness, maxBrightness);
}

/* --------------------------------------------------------------------
Function pointer stuff
----------------------------------------------------------------------
 */


void nullFn () {} ;


void sunset() {
  lcd_clear();
  lampOn();
  lampInUse = 0;		/* dare you to create race condition! */
  setTime(&fadeTime, "Sunset duration:");
  lcd_clear();
  lcd_print("Sun setting...");
  digitalwrite(backlight, 0);
  fade(fadeTime, 0);	/* don't care about return value */
  lampOff();
  lampInUse = 0;		/* ditto! */
  enterMainMenu();
  
  return;
}

void timeout() {
  menuTimeout = 1;
}

void systemInfo() {
  lcd_clear();
  lcd_print("Firmware ");
  lcd_print(firmwareVersion);
  lcd_setCursor(0,1);
  lcd_print("2e0byo/Pinguino");
  pollButton(enterButton, 0, 1);
}

void factorySettings() {
  /* set everyting which is eepromed for first run */
  alarmEnabled = 0;
  ringOn = 1;
  writeEepromFlags;
  alarmTime = 0;
  EEPROM_write24(eepromAlarmTime, alarmTime);
  
}
/* Menu functions */

void nextMenuEntry () {
  idleCount = 0;
  if (index < menuSize) {
    currentMenu++;
    index++;
  } else {
    for (u8 j=0; j<menuSize; j++) {
      currentMenu--;
    }
    index = 0;
  }
}

void previousMenuEntry () {
  if (index > 0) {
    currentMenu--;
    index--;
  } else {
    index = menuSize;
    for (u8 j=0; j<menuSize; j++) {
      currentMenu++;
    }
  }
}



menu setupMenu [] = {
		     {"Set Time", setClock, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		     {"Set Min Bright", setMinBrightness, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		     {"Set Max Bright", setMaxBrightness, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		     {"Factory Reset", factorySettings, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		     {"System Info", systemInfo, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry}
};


void enterSetupMenu() {
  index = 0;
  currentMenu = setupMenu;
  menuSize = 4;
}

menu alarmMenu [] = {
		     {"Enable/Disable?", toggleAlarmEnabled, printAlarmStateLowerLine, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry}, /* do we need to move lines for print alarm state? */
		     {"Set Alarm Time", setAlarm, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		     {"Set Fade Time", setFadeOnMinutes, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry}
};

void enterAlarmMenu() {
  index = 0;
  currentMenu = alarmMenu;
  menuSize = 2;
}

menu mainMenu [] = {
		    {"", toggleLamp, homeScreen, backlightOff, editSnooze, adjustLamp, adjustLamp},
		    {"Start Sunset", sunset, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		    {"Setup Alarm", enterAlarmMenu, printAlarmStateLowerLine, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		    {"System Setup", enterSetupMenu, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
		    {"Set Alarm Time", setAlarm, nullFn, timeout, enterMainMenu, nextMenuEntry, previousMenuEntry}
};

void enterMainMenu() {
  index = 0;
  currentMenu = mainMenu;
  menuSize = 4;
}


void homeScreen() {
  lcd_home();
  if (clockReady) {
    printAlarmState();
    printTime();
    if (savedAlarmTime) {
      printSnooze();
    }
  }
}

void menuLoop(){

  /* 
     while not enter button:
     - if up, move up, print, then execute displayFunc
     - if down, ditto
     Then: 
     - if long press: run exitCurrentmenu
     - else run enterFunc

     The menuLoop doesn't die unless displayMenu is toggled from
     elsewhere.  Currently this can only happen if an alarm elapses
     (in which case we return to the main loop for a bit).

 */

  u8 countFlag = 0;
  menuChange = 1;
  while (!alarmFlag) {
    if (menuChange) {
      lcd_clear();
      lcd_print(currentMenu->name);
    }
    menuChange = 1;


    if (countFlag == 2) {
      debounceButton(enterButton);
      countFlag = 0;
    }

    u8 ret;

    while (!alarmFlag && !menuTimeout && digitalread(enterButton)) {
      
      currentMenu->displayFunc();	/* mostly does nothing, homeScreen called here */

      if (ret == 2) {
	debounceButton(upButton);
	debounceButton(downButton);
      }

      idleCount++;

      if (idleCount > 600) {
	currentMenu->timeoutFunc();
	idleCount = 0;
      }
  
      if (!digitalread(backlight)){
        if  ( !digitalread(upButton) || !digitalread(downButton)) {
          digitalwrite(backlight, HIGH);
	  idleCount = 0;
          while (!digitalread(upButton) || !digitalread(downButton)) {
	    Delayms(100);
          }
	  Delayms(100);		/* debounce */
	  continue;
	}
      }

      ret = pollButton(downButton, 1, 0);
      if (ret == 1) {
        previousMenuEntry();
      } else if (ret == 2) {
	currentMenu->longUpFunction();
      }


      if (!ret) {
	ret = pollButton(upButton, 1, 0);
	if (ret == 1) {
	  nextMenuEntry();
	} else if (ret == 2) {
	  currentMenu->longUpFunction();
	}
      }

      if (ret) {
	lcd_clear();
	lcd_print(currentMenu->name);
	currentMenu->displayFunc();
      } else {
	Delayms(100);
      }
    }
    
    if (menuTimeout) {
      menuTimeout = 0;
      enterMainMenu();
      continue;
    }
    
    countFlag = pollButton(enterButton, 1, 0);
    if (countFlag == 2) {
      currentMenu->exitCurrentMenu();
    } else if (countFlag == 1) {
      currentMenu->enterFunc();
    }
    else {
      Delayms(50);		/* Main Loop Delay */
    }
  }
}

void snooze() {
  /* Setup a snooze alarm.  This stores the current alarm and fade on
     value, and then replaces them with a new alarm. */
  
  lcd_clear();
  lcd_print("Snooze time:");
  lcd_setCursor(0,1);
  lcd_printf("%02u",snoozeMinutes);
  lcd_print(" Minutes");
  lcd_setCursor(0,1);
  while (!digitalread(enterButton)) {
    Delayms(100);
  }

  snoozeMinutes = setValue(snoozeMinutes, 3, 59, 0);
  if (!savedAlarmTime) {
    savedAlarmTime = alarmTime;
  }
  alarmTime = time + snoozeMinutes * 60;
  if (alarmTime > day) {
    alarmTime = snoozeMinutes * 60 - time;
  }
  savedFadeOnMinutes = fadeOnMinutes;
  if (snoozeMinutes > 5) {
    fadeOnMinutes = 5;
  }
  else {
    fadeOnMinutes = 2;
  }

  adjustAlarm();
}

void editSnooze() {
  if (savedAlarmTime) {
    snooze();
  }
}

void setup() {
  pinmode(lamp,OUTPUT);
  digitalwrite(lamp, HIGH);
  Audio_init(TAPEQUALITY);
  analogwrite_init();
  pinmode(upButton, INPUT);
  pinmode(downButton, INPUT);
  pinmode(enterButton, INPUT);
  lcd_pins(7,1,0,0,0,0,2,3,4,5); 
  
  lcd_begin(16, 2);
  pinmode(backlight, OUTPUT);
  
  OnChangePin0(tick, INT_FALLING_EDGE);
  digitalwrite(backlight, HIGH);
  lcd_clear();
  stopClock = 1;

  readEepromFlags();

  time = 0;
  setClock();
  stopClock = 0;		
  minBrightness = EEPROM_read16(eepromMinBrightness);
  if (minBrightness > 1023) {
    minBrightness = 0;
  }
  maxBrightness = EEPROM_read16(eepromMaxBrightness);
  if (maxBrightness > 1023) {
    maxBrightness = 1023;
  }
  fadeOnMinutes = EEPROM_read8(eepromFadeOnMinutes);
  if (fadeOnMinutes > 59) {
    fadeOnMinutes = 30;
  }

  alarmTime = EEPROM_read24(eepromAlarmTime);
  if (alarmTime > day) {
    alarmTime = 0;
    alarmEnabled = 0;
  }
  adjustAlarm();
}

void printInterruptStatus() {
  lcd_clear();
  lcd_printf("%u:%u",intUsed[INT_TMR0], intCount[INT_TMR0]);
  lcd_setCursor(0,1);
  lcd_printf("%u", T0CON);
  pollButton(enterButton, 0, 1);
}

void loop() {
  enterMainMenu();
  menuLoop();			/* we only leave when alarm elapses */

  /* ring */
  lcd_clear();
  lcd_home();
  u8 status = fadeOn();
  digitalwrite(backlight, HIGH);
  if (!status){
    status = ring();
  }
  if (savedAlarmTime) {
    alarmTime = savedAlarmTime;
    savedAlarmTime = 0;
    adjustAlarm();
    fadeOnMinutes = savedFadeOnMinutes;
  }
  if (status == 2) {
    snooze();
  }
  
  
  digitalwrite(backlight, HIGH);

  /* end */
  lampOff();
  alarmFlag = 0;
  /* in case corrupted */
  lcd_begin(16, 2);
  lcd_clear();
  lcd_home();
}
