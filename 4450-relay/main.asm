;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.5.0/*rc1*/ #9248 (Jun 25 2015) (Linux)
; This file was generated Sat Mar 28 00:57:35 2020
;--------------------------------------------------------
; PIC16 port for the Microchip 16-bit core micros
;--------------------------------------------------------
	list	p=18f2550
	radix	dec


;--------------------------------------------------------
; public variables in this module
;--------------------------------------------------------
	global	___uflags
	global	__cpu_clock_
	global	__entry
	global	__startup
	global	_main
	global	_IO_init
	global	_IO_digital
	global	_test
	global	_setup
	global	_sum
	global	_subtract
	global	_mul
	global	_loop

;--------------------------------------------------------
; extern variables in this module
;--------------------------------------------------------
	extern	_UFRMLbits
	extern	_UFRMHbits
	extern	_UIRbits
	extern	_UIEbits
	extern	_UEIRbits
	extern	_UEIEbits
	extern	_USTATbits
	extern	_UCONbits
	extern	_UADDRbits
	extern	_UCFGbits
	extern	_UEP0bits
	extern	_UEP1bits
	extern	_UEP2bits
	extern	_UEP3bits
	extern	_UEP4bits
	extern	_UEP5bits
	extern	_UEP6bits
	extern	_UEP7bits
	extern	_UEP8bits
	extern	_UEP9bits
	extern	_UEP10bits
	extern	_UEP11bits
	extern	_UEP12bits
	extern	_UEP13bits
	extern	_UEP14bits
	extern	_UEP15bits
	extern	_PORTAbits
	extern	_PORTBbits
	extern	_PORTCbits
	extern	_PORTEbits
	extern	_LATAbits
	extern	_LATBbits
	extern	_LATCbits
	extern	_DDRAbits
	extern	_TRISAbits
	extern	_DDRBbits
	extern	_TRISBbits
	extern	_DDRCbits
	extern	_TRISCbits
	extern	_OSCTUNEbits
	extern	_PIE1bits
	extern	_PIR1bits
	extern	_IPR1bits
	extern	_PIE2bits
	extern	_PIR2bits
	extern	_IPR2bits
	extern	_EECON1bits
	extern	_RCSTAbits
	extern	_TXSTAbits
	extern	_T3CONbits
	extern	_CMCONbits
	extern	_CVRCONbits
	extern	_CCP1ASbits
	extern	_ECCP1ASbits
	extern	_CCP1DELbits
	extern	_ECCP1DELbits
	extern	_BAUDCONbits
	extern	_BAUDCTLbits
	extern	_CCP2CONbits
	extern	_CCP1CONbits
	extern	_ADCON2bits
	extern	_ADCON1bits
	extern	_ADCON0bits
	extern	_SSPCON2bits
	extern	_SSPCON1bits
	extern	_SSPSTATbits
	extern	_T2CONbits
	extern	_T1CONbits
	extern	_RCONbits
	extern	_WDTCONbits
	extern	_HLVDCONbits
	extern	_LVDCONbits
	extern	_OSCCONbits
	extern	_T0CONbits
	extern	_STATUSbits
	extern	_INTCON3bits
	extern	_INTCON2bits
	extern	_INTCONbits
	extern	_STKPTRbits
	extern	_stack_end
	extern	_UFRM
	extern	_UFRML
	extern	_UFRMH
	extern	_UIR
	extern	_UIE
	extern	_UEIR
	extern	_UEIE
	extern	_USTAT
	extern	_UCON
	extern	_UADDR
	extern	_UCFG
	extern	_UEP0
	extern	_UEP1
	extern	_UEP2
	extern	_UEP3
	extern	_UEP4
	extern	_UEP5
	extern	_UEP6
	extern	_UEP7
	extern	_UEP8
	extern	_UEP9
	extern	_UEP10
	extern	_UEP11
	extern	_UEP12
	extern	_UEP13
	extern	_UEP14
	extern	_UEP15
	extern	_PORTA
	extern	_PORTB
	extern	_PORTC
	extern	_PORTE
	extern	_LATA
	extern	_LATB
	extern	_LATC
	extern	_DDRA
	extern	_TRISA
	extern	_DDRB
	extern	_TRISB
	extern	_DDRC
	extern	_TRISC
	extern	_OSCTUNE
	extern	_PIE1
	extern	_PIR1
	extern	_IPR1
	extern	_PIE2
	extern	_PIR2
	extern	_IPR2
	extern	_EECON1
	extern	_EECON2
	extern	_EEDATA
	extern	_EEADR
	extern	_RCSTA
	extern	_TXSTA
	extern	_TXREG
	extern	_RCREG
	extern	_SPBRG
	extern	_SPBRGH
	extern	_T3CON
	extern	_TMR3
	extern	_TMR3L
	extern	_TMR3H
	extern	_CMCON
	extern	_CVRCON
	extern	_CCP1AS
	extern	_ECCP1AS
	extern	_CCP1DEL
	extern	_ECCP1DEL
	extern	_BAUDCON
	extern	_BAUDCTL
	extern	_CCP2CON
	extern	_CCPR2
	extern	_CCPR2L
	extern	_CCPR2H
	extern	_CCP1CON
	extern	_CCPR1
	extern	_CCPR1L
	extern	_CCPR1H
	extern	_ADCON2
	extern	_ADCON1
	extern	_ADCON0
	extern	_ADRES
	extern	_ADRESL
	extern	_ADRESH
	extern	_SSPCON2
	extern	_SSPCON1
	extern	_SSPSTAT
	extern	_SSPADD
	extern	_SSPBUF
	extern	_T2CON
	extern	_PR2
	extern	_TMR2
	extern	_T1CON
	extern	_TMR1
	extern	_TMR1L
	extern	_TMR1H
	extern	_RCON
	extern	_WDTCON
	extern	_HLVDCON
	extern	_LVDCON
	extern	_OSCCON
	extern	_T0CON
	extern	_TMR0
	extern	_TMR0L
	extern	_TMR0H
	extern	_STATUS
	extern	_FSR2L
	extern	_FSR2H
	extern	_PLUSW2
	extern	_PREINC2
	extern	_POSTDEC2
	extern	_POSTINC2
	extern	_INDF2
	extern	_BSR
	extern	_FSR1L
	extern	_FSR1H
	extern	_PLUSW1
	extern	_PREINC1
	extern	_POSTDEC1
	extern	_POSTINC1
	extern	_INDF1
	extern	_WREG
	extern	_FSR0L
	extern	_FSR0H
	extern	_PLUSW0
	extern	_PREINC0
	extern	_POSTDEC0
	extern	_POSTINC0
	extern	_INDF0
	extern	_INTCON3
	extern	_INTCON2
	extern	_INTCON
	extern	_PROD
	extern	_PRODL
	extern	_PRODH
	extern	_TABLAT
	extern	_TBLPTR
	extern	_TBLPTRL
	extern	_TBLPTRH
	extern	_TBLPTRU
	extern	_PC
	extern	_PCL
	extern	_PCLATH
	extern	_PCLATU
	extern	_STKPTR
	extern	_TOS
	extern	_TOSL
	extern	_TOSH
	extern	_TOSU
	extern	_cinit

;--------------------------------------------------------
;	Equates to used internal registers
;--------------------------------------------------------
STATUS	equ	0xfd8
FSR1L	equ	0xfe1
FSR2L	equ	0xfd9
POSTDEC1	equ	0xfe5
PREINC1	equ	0xfe4


	idata
__cpu_clock_	db	0x00, 0x6c, 0xdc, 0x02
___uflags	db	0x00

udata_main_0	udata
_setup_p_1_7	res	12

;--------------------------------------------------------
; interrupt vector
;--------------------------------------------------------

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
; ; Starting pCode block
S_main___entry	code	0X000C00
__entry:
	goto __startup
	
; I code from now on!
; ; Starting pCode block
S_main__main	code
_main:
;	.line	188; /home/john/Pinguino/v13/source/main.c	RCONbits.IPEN = 1;              // Enables priority levels on
	BSF	_RCONbits, 7
;	.line	193; /home/john/Pinguino/v13/source/main.c	if (RCONbits.NOT_POR == 0)
	BTFSC	_RCONbits, 1
	BRA	_00141_DS_
;	.line	195; /home/john/Pinguino/v13/source/main.c	RCONbits.NOT_POR = 1;       // POR and BOR flags must be cleared by
	BSF	_RCONbits, 1
;	.line	196; /home/john/Pinguino/v13/source/main.c	RCONbits.NOT_BOR = 1;       // software to allow a new detection
	BSF	_RCONbits, 0
_00141_DS_:
;	.line	281; /home/john/Pinguino/v13/source/main.c	if (OSCCONbits.SCS > 0x01)
	MOVF	_OSCCONbits, W
	ANDLW	0x03
; #	MOVWF	r0x00
; #	MOVF	r0x00, W
	ADDLW	0x80
	ADDLW	0x7e
	BNC	_00146_DS_
_00142_DS_:
;	.line	283; /home/john/Pinguino/v13/source/main.c	while (!OSCCONbits.IOFS);
	BTFSS	_OSCCONbits, 2
	BRA	_00142_DS_
_00146_DS_:
;	.line	333; /home/john/Pinguino/v13/source/main.c	IO_init();
	CALL	_IO_init
;	.line	334; /home/john/Pinguino/v13/source/main.c	IO_digital();
	CALL	_IO_digital
;	.line	396; /home/john/Pinguino/v13/source/main.c	setup();
	CALL	_setup
_00148_DS_:
;	.line	415; /home/john/Pinguino/v13/source/main.c	loop();
	CALL	_loop
	BRA	_00148_DS_
	RETURN	

; ; Starting pCode block
S_main___startup	code
__startup:
	; Initialize the stack pointer
	lfsr 1, _stack_end
	lfsr 2, _stack_end
	; 1st silicon does not do this on POR
	clrf _TBLPTRU, 0
	; Initialize the flash memory access configuration.
	; This is harmless for non-flash devices, so we do it on all parts.
	bsf 0xa6, 7, 0 ; EECON1.EEPGD = 1, TBLPTR accesses program memory
	bcf 0xa6, 6, 0 ; EECON1.CFGS = 0, TBLPTR accesses program memory
	; TBLPTR = &cinit
	movlw low(_cinit)
	movwf _TBLPTRL, 0
	movlw high(_cinit)
	movwf _TBLPTRH, 0
	movlw upper(_cinit)
	movwf _TBLPTRU, 0
	; 0x05 = cinit.num_init
	tblrd*+
	movff _TABLAT, 0x05
	tblrd*+
	movff _TABLAT, (0x05 + 1)
	; while (0x05)
	bra entry_loop_dec
entry_loop:
	; Count down so we only have to look up the data in _cinit once.
	; At this point we know that TBLPTR points to the top of the current
	; entry in _cinit, so we can just start reading the from, to, and
	; size values.
	; Read the source address low.
	; 0x00 = 0x07 ->from;
	tblrd*+
	movff _TABLAT, 0x00
	; source address high
	tblrd*+
	movff _TABLAT, (0x00 + 1)
	; source address upper
	tblrd*+
	movff _TABLAT, (0x00 + 2)
	; Skip 0 byte since it is stored as 0 32bit int.
	tblrd*+
	; Read the destination address directly into FSR0
	; destination address low.
	; FSR0 = (unsigned short)0x07 ->to;
	tblrd*+
	movff _TABLAT, _FSR0L
	; destination address high
	tblrd*+
	movff _TABLAT, _FSR0H
	; Skip two bytes since it is stored as 0 32bit int.
	tblrd*+
	tblrd*+
	; Read the size of data to transfer to destination address.
	; 0x03 = (unsigned short)0x07 ->size;
	tblrd*+
	movff _TABLAT, 0x03
	tblrd*+
	movff _TABLAT, (0x03 + 1)
	; Skip two bytes since it is stored as 0 32bit int.
	tblrd*+
	tblrd*+
	; 0x00 = 0x07 ->from;
	; FSR0 = (unsigned short)0x07 ->to;
	; 0x03 = (unsigned short)0x07 ->size;
	; The table pointer now points to the next entry. Save it
	; off since we will be using the table pointer to do the copying
	; for the entry.
	; 0x07 = TBLPTR
	movff _TBLPTRL, 0x07
	movff _TBLPTRH, (0x07 + 1)
	movff _TBLPTRU, (0x07 + 2)
	; Now assign the source address to the table pointer.
	; TBLPTR = 0x00
	movff 0x00, _TBLPTRL
	movff (0x00 + 1), _TBLPTRH
	movff (0x00 + 2), _TBLPTRU
	bra copy_loop_dec
copy_loop:
	tblrd*+
	movff _TABLAT, _POSTINC0
copy_loop_dec:
	; while (--0x03);
	; Decrement and test the byte counter.
	; The cycle ends when the value of counter reaches the -1.
	decf 0x03, f, 0
	bc copy_loop
	decf (0x03 + 1), f, 0
	bc copy_loop
	; Restore the table pointer for the next entry.
	; TBLPTR = 0x07
	movff 0x07, _TBLPTRL
	movff (0x07 + 1), _TBLPTRH
	movff (0x07 + 2), _TBLPTRU
entry_loop_dec:
	; while (--0x05);
	; Decrement and test the entry counter.
	; The cycle ends when the value of counter reaches the -1.
	decf 0x05, f, 0
	bc entry_loop
	decf (0x05 + 1), f, 0
	bc entry_loop
	
;	.line	240; /home/john/Pinguino/v13/source/crt0i.c	main ();
	CALL	_main
lockup:
	; Returning from main will lock up.
	bra lockup
	
; ; Starting pCode block
S_main__loop	code
_loop:
;	.line	29; /home/john/Pinguino/v13/source/user.c	}
	RETURN	

; ; Starting pCode block
S_main__setup	code
_setup:
;	.line	19; /home/john/Pinguino/v13/source/user.c	p[0] = &sum; 
	MOVLW	LOW(_sum)
	BANKSEL	_setup_p_1_7
	MOVWF	_setup_p_1_7, B
	MOVLW	HIGH(_sum)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 1), B
	MOVLW	UPPER(_sum)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 2), B
;	.line	20; /home/john/Pinguino/v13/source/user.c	p[1] = &subtract; 
	MOVLW	LOW(_subtract)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 3), B
	MOVLW	HIGH(_subtract)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 4), B
	MOVLW	UPPER(_subtract)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 5), B
;	.line	21; /home/john/Pinguino/v13/source/user.c	p[2] = &mul; 
	MOVLW	LOW(_mul)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 6), B
	MOVLW	HIGH(_mul)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 7), B
	MOVLW	UPPER(_mul)
; removed redundant BANKSEL
	MOVWF	(_setup_p_1_7 + 8), B
	RETURN	

; ; Starting pCode block
S_main__test	code
_test:
;	.line	9; /home/john/Pinguino/v13/source/user.c	}
	RETURN	

; ; Starting pCode block
S_main__IO_digital	code
_IO_digital:
;	.line	130; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	ADCON1 = 0x0F;              // AN0 to AN12 Digital I/O
	MOVLW	0x0f
	MOVWF	_ADCON1
;	.line	131; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	CMCON  = 0x07;              // Comparators as Digital I/O
	MOVLW	0x07
	MOVWF	_CMCON
	RETURN	

; ; Starting pCode block
S_main__IO_init	code
_IO_init:
;	.line	56; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	LATA  = 0x00;
	CLRF	_LATA
;	.line	57; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	LATB  = 0x00;
	CLRF	_LATB
;	.line	75; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	LATC  = 0x00;
	CLRF	_LATC
;	.line	86; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISA = 0x00;
	CLRF	_TRISA
;	.line	88; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISB = 0x00;
	CLRF	_TRISB
;	.line	98; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISCbits.TRISC0 = 0x00;
	BCF	_TRISCbits, 0
;	.line	99; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISCbits.TRISC1 = 0x00;
	BCF	_TRISCbits, 1
;	.line	100; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISCbits.TRISC2 = 0x00;
	BCF	_TRISCbits, 2
;	.line	101; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISCbits.TRISC6 = 0x00;
	BCF	_TRISCbits, 6
;	.line	102; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	TRISCbits.TRISC7 = 0x00;
	BCF	_TRISCbits, 7
;	.line	113; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	PORTA  = 0x00;
	CLRF	_PORTA
;	.line	114; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	PORTB  = 0x00;
	CLRF	_PORTB
;	.line	115; /home/john/Pinguino/pinguino-libraries/p8/include/pinguino/core/io.c	PORTC  = 0x00;
	CLRF	_PORTC
	RETURN	



; Statistics:
; code size:	  344 (0x0158) bytes ( 0.26%)
;           	  172 (0x00ac) words
; udata size:	   12 (0x000c) bytes ( 0.67%)
; access size:	    0 (0x0000) bytes


	end
