/* Declarations */
void setBrightness(unsigned short *br, char instruction[16], char name[12],
                   unsigned short max, unsigned short min);
void second();
void writeEepromFlags();
unsigned char pollButton(unsigned char button, char waitLongPress, char wait);
void debounceButton(unsigned char button);
void readEepromFlags();
unsigned char ring();
void printAlarmState();
void timeToHMS(__uint24 t, unsigned char *h, unsigned char *m,
               unsigned char *s);
void writeEepromFlags();
void homeScreen();
void snooze();
void nap();
void printSnooze();
void editSnoozeOrToggleLamp();
void enterMainMenu();
void printTime();

/* includes */
#include "backlight_hal.c"
#include "hardware.c"
#include "lamp_hal.c"
#include "lcd_chars.h"
#include "mains.c"

typedef void (*voidFnPointer)();

/* vars */
char firmwareVersion[] = "v1.5.2";

volatile char clockReady;
char fadeReady;
bit stopClock = 0;
bit stopRing = 0;
unsigned char clockInterrupt;
unsigned char lampInterrupt;
bit lampState = 0;
unsigned char menuTimeout = 0;
unsigned char index = 0;
bit menuChange;
bit lampInUse;
unsigned char pps;
unsigned char mainsFreq = 50;

unsigned short maxBrightness = 200;
unsigned short minBrightness;
unsigned short brightness = 0;

#define day 86399

volatile __uint24 time = 0;
__uint24 fadeTime;
__uint24 alarmTime;
volatile __uint24 realAlarmTime;
__uint24 savedAlarmTime;
unsigned char alarmH, alarmM, alarmS; /* for printing */
unsigned char savedFadeOnMinutes;
unsigned char fadeOnMinutes;
unsigned char snoozeMinutes = 3;

unsigned char slowDelay_ms = 50;
unsigned char medDelay_ms = 20;
unsigned char fastDelay_ms = 10;

bit alarmEnabled;
bit alarmFlag;
bit ringOn;
bit timeoutFlag;

static const unsigned char eepromMinBrightness = 0;
static const unsigned char eepromMaxBrightness = 2;
static const unsigned char eepromFadeOnMinutes = 4;
static const unsigned char eepromAlarmTime = 7; /* 24-bit = 3 bytes */
static const unsigned char eepromFlags = 10;
static const unsigned char eepromSlowDelay = 11;
static const unsigned char eepromMedDelay = 12;
static const unsigned char eepromFastDelay = 13;


typedef struct {
  int freq;
  int rest;
} Note;

Note melody[] = {{NOTE_G6, 4}, {NOTE_G6, 4}};

void writeEepromFlags() {
  unsigned char flags;
  flags = flags | alarmEnabled;
  flags = (flags << 1) | ringOn;
  EEPROM_write8(eepromFlags, flags);
}

typedef struct {
  unsigned char *name;
  voidFnPointer enterFunc;
  voidFnPointer displayFunc;      /* function to run on switching to screen */
  voidFnPointer timeoutFunc;      /* Timeout */
  voidFnPointer exitCurrentMenu;  /* Long Press */
  voidFnPointer longDownFunction; /* Hold Down Button */
  voidFnPointer longUpFunction;   /* Hold Up Button */
} menu;

menu *currentMenu = NULL;
volatile unsigned short idleCount = 0;
volatile unsigned char menuSize;



/**
 * @brief Read Flags from Eeprom.
 * @details Read flags saved into Eeprom and shift them into ram.
 */
void readEepromFlags() {
  unsigned char flags = EEPROM_read8(eepromFlags);
  ringOn = flags & 1;
  alarmEnabled = (flags >> 1) & 1;
}

/* Hardware functions */
unsigned char ring() {
  if (!ringOn) {
    unsigned char ret = pollButton(enterButton, 1, 1);
    return ret;
  }
  stopRing = 0;
  unsigned short volume = 8;
  unsigned short timeout = 0;
  while (!stopRing) {
    /* total time taken ~ .1+.5+1 = 1.6s */
    /* timeout after 15m = 900s /1.6 ~ 562loops */
    if (++timeout > 562) {
      return 3;
    }

    for (unsigned char thisNote = 0; thisNote < 2; thisNote++) {
      analogwrite(LINEOUT, volume);
      for (unsigned char i = 0; i <= 10; ++i) {
        Delayms(10);
        if (clockReady)
          printTime();
        if (!digitalread(enterButton)) {
          stopRing = 1;
          break;
        }
      }
      analogwrite(LINEOUT, 0);
      for (unsigned char i = 0; i <= 5; ++i) {
        if (stopRing)
          break;
        Delayms(10);
        if (clockReady)
          printTime();
        if (!digitalread(enterButton)) {
          stopRing = 1;
          break;
        }
      }
      if (stopRing)
        break;
    }

    for (unsigned char i = 0; i <= 100; ++i) {
      Delayms(10);
      if (clockReady)
        printTime();
      if (!digitalread(enterButton)) {
        stopRing = 1;
        break;
      }
    }

    if (volume < 512)
      volume += 8;
  }
  unsigned char ret = pollButton(enterButton, 1, 1);
  return ret;
}

void resetDisplay() {
  lcd_begin(16, 2);
  lcd_clear();
}

void second() {
  --pps;
  if (pps) {
    return; /* pps > 0 */
  } else {
    pps = mainsFreq;
  }

  if (stopClock) {
    return;
  }

  time++;
  if (time > day) {
    time = 0;
  }
  clockReady = 1;

  if (alarmEnabled && realAlarmTime == time) {
    alarmFlag = 1;
  }
}

void printAlarmState() {
  if (savedAlarmTime) {
    lcd_print("Snoozing...");
    return;
  }
  if (alarmEnabled) {
    lcd_print("Alarm On: ");
  } else {
    lcd_print("Alarm Off:");
  }
  lcd_printf(" %02u:%02u", alarmH, alarmM);
}

void timeToHMS(__uint24 t, unsigned char *h, unsigned char *m,
               unsigned char *s) {
  uldiv_t result;
  result = uldiv(t, 3600);
  *h = (unsigned char)result.quot;
  result = uldiv(result.rem, 60);
  *m = (unsigned char)result.quot;
  *s = (unsigned char)result.rem;
}

void printTime() {
  lcd_setCursor(0, 1);
  unsigned char h, m, s;
  timeToHMS(time, &h, &m, &s);
  lcd_printf("    %02u:%02u:%02u    ", h, m, s);
  clockReady = 0;
}

void printSnooze() { /* just prints time to alarm */
  lcd_setCursor(11, 0);
  unsigned char h, m, s;
  timeToHMS(alarmTime - time, &h, &m, &s);
  lcd_printf("%02u:%02u", m, s);
}

void printAlarmStateLowerLine() {
  /* print arrows */
  lcd_setCursor(15, 1);
  lcd_write(UP_ARROW);
  lcd_setCursor(0, 1);
  lcd_write(DOWN_ARROW);
  lcd_setCursor(2, 1);
  if (alarmEnabled) {
    lcd_print("(On: ");
  } else {
    lcd_print("(Off:");
  }
  lcd_printf(" %02u:%02u)", alarmH, alarmM);
}

void fadeISR() { fadeReady = 1; }

unsigned char fade(__uint24 t, char direction) {
  unsigned short steps = 0;
  if (brightness > minBrightness)
    steps = brightness - minBrightness;
  else
    steps = maxBrightness - minBrightness;
  uldiv_t result;

  /* can we use system clock as ticker? */
  char *readyFlag = NULL;
  unsigned char fadeInterrupt = 0;
  if (t >= steps) {
    result = uldiv(t, steps);
    readyFlag = &clockReady;
  } else { /* use 10ms ticker */
    result = uldiv(t * 100, steps);
    fadeInterrupt = OnTimer3(fadeISR, INT_MILLISEC, 9);
    readyFlag = &fadeReady;
  }
  unsigned char endFlag = 0;

  while (1) {
    if (direction) {
      if (brightness == maxBrightness)
        break;
      ++brightness;
    } else {
      if (brightness == minBrightness)
        break;
      --brightness;
    }
    setLampBrightness(&brightness);
    unsigned short pause = 0;

    while (pause < result.quot) {
      while (!*readyFlag) {
        endFlag = pollButton(enterButton, 1, 0);
        if (endFlag) {
          if (fadeInterrupt)
            IntDisable(fadeInterrupt);
          return endFlag;
        }
      }
      if (clockReady)
        printTime();
      *readyFlag = 0;
      pause++;
    }
    if (result.rem > 0) {
      /* Add remainder tick to this step*/
      while (!*readyFlag) {
        endFlag = pollButton(enterButton, 1, 0);
        if (endFlag) {
          if (fadeInterrupt)
            IntDisable(fadeInterrupt);
          return endFlag;
        }
      }

      if (clockReady)
        printTime();
      *readyFlag = 0;
      result.rem--;
    }
  }
  if (fadeInterrupt)
    IntDisable(fadeInterrupt);
  return 0;
}

unsigned char fadeOn() {
  unsigned char ret;
  brightness = minBrightness;

  ret = fade(fadeOnMinutes * 60, 1);
  return ret;
}

void toggleBacklight() {
  if (backlightStatus) {
    backlightOff();
  } else {
    backlightOn();
  }
}

char toggleValue(char value, char yes[16], char no[16]) {
  unsigned short timeoutCounter = 0;

  lcd_clear();
  lcd_setCursor(0, 1);
  if (value) {
    lcd_print(yes);
  } else {
    lcd_print(no);
  }
  lcd_setCursor(0, 1);

  while (!timeoutFlag && digitalread(enterButton)) {
    if (!digitalread(upButton) || !digitalread(downButton)) {
      timeoutCounter = 0;
      if (value) {
        value = 0;
        lcd_print(no);
      } else {
        value = 1;
        lcd_print(yes);
      }
      lcd_setCursor(0, 1);
      while (!digitalread(upButton) || !digitalread(downButton)) {
        Delayms(100);
      }
    }

    if (++timeoutCounter == 300)
      timeoutFlag = 1;

    Delayms(100); /* Main Loop Delay */
  }

  if (!timeoutFlag)
    debounceButton(enterButton);

  timeoutFlag = 0;
  return value;
}

void toggleAlarmEnabled() {
  alarmEnabled = toggleValue(alarmEnabled, "Alarm Enabled ", "Alarm Disabled");
  writeEepromFlags();
}

unsigned short setValue(unsigned short value, unsigned short min,
                        unsigned short max, unsigned char col,
                        unsigned char row) {
  unsigned short timeoutCounter = 0;

  /* print arrows */
  lcd_setCursor(15, row);
  lcd_write(UP_ARROW);
  lcd_setCursor(0, row);
  lcd_write(DOWN_ARROW);
  lcd_setCursor(col, row);

  unsigned char ret, ret2;
  while (!timeoutFlag && digitalread(enterButton)) {
    if (!ret && !ret2) {
      ret = pollButton(upButton, 1, 0);
    }
    if (ret == 1) {
      if (value < max) {
        value += 1;
      } else {
        value = min;
      }
    } else if (ret == 2) {
      if (value <= max - 2) {
        value += 2;
      } else {
        value = min;
      }
    }

    if (!ret && !ret2) {
      ret2 = pollButton(downButton, 1, 0);
    }
    if (ret2 == 1) {
      if (value > min) {
        value -= 1;
      } else {
        value = max;
      }
    } else if (ret2 == 2) {
      if (value >= min + 2) {
        value -= 2;
      } else {
        value = max;
      }
    }

    if (ret || ret2) {
      timeoutCounter = 0;
      if (max > 999) {
        lcd_printf("%04u", value);
      } else if (max > 99) {
        lcd_printf("%03u", value);
      } else {
        lcd_printf("%02u", value);
      }
      lcd_setCursor(col, 1);
    }

    if (digitalread(downButton) && digitalread(upButton)) {
      ret = 0;
      ret2 = 0;
    }
    if (++timeoutCounter == 300)
      timeoutFlag = 1;

    Delayms(100);
  }
  if (!timeoutFlag)
    debounceButton(enterButton);

  timeoutFlag = 0;

  return value;
}

void setTime(__uint24 *t, char name[16]) {

  lcd_clear();
  lcd_print(name);
  lcd_setCursor(4, 1);
  unsigned char h, m, s;
  timeToHMS(*t, &h, &m, &s);

  lcd_printf("%02u:%02u:%02u", h, m, s);
  lcd_cursor();
  h = setValue(h, 0, 23, 4, 1);
  lcd_setCursor(7, 1);
  m = setValue(m, 0, 59, 7, 1);
  lcd_setCursor(10, 1);
  s = setValue(s, 0, 59, 10, 1);
  lcd_noCursor();
  *t = s + ((__uint24)m * 60) + ((__uint24)h * 3600);
}

void setClock() {
  lcd_clear();
  stopClock = 1;
  setTime(&time, "Time:");
  stopClock = 0;
}

void adjustAlarm() {
  realAlarmTime = alarmTime - fadeOnMinutes * 60;
  if (realAlarmTime > day) { /* overflowed */
    realAlarmTime = day - fadeOnMinutes * 60 + alarmTime;
  }
  timeToHMS(alarmTime, &alarmH, &alarmM, &alarmS);
}

void setAlarm() {
  setTime(&alarmTime, "Alarm:");
  EEPROM_write24(eepromAlarmTime, alarmTime);
  adjustAlarm();
  ringOn = toggleValue(ringOn, "Ring Enabled", "Ring Disabled");
  writeEepromFlags();
}

void setFadeOnMinutes() {
  lcd_clear();
  lcd_print("Fade on time:");
  lcd_setCursor(4, 1);
  lcd_printf("%02u", fadeOnMinutes);
  lcd_print(" Minutes");
  fadeOnMinutes = setValue(fadeOnMinutes, 0, 59, 4, 1);
  EEPROM_write8(eepromFadeOnMinutes, fadeOnMinutes);
}

void setSlowDelay() {
  lcd_clear();
  lcd_print("Slow Delay:");
  lcd_setCursor(4, 1);
  lcd_printf("%02u mS", slowDelay_ms);
  slowDelay_ms = setValue(slowDelay_ms, 0, 99, 4, 1);
  EEPROM_write8(eepromSlowDelay, slowDelay_ms);
}

void setMedDelay() {
  lcd_clear();
  lcd_print("Medium Delay:");
  lcd_setCursor(4, 1);
  lcd_printf("%02u mS", medDelay_ms);
  medDelay_ms = setValue(medDelay_ms, 0, 99, 4, 1);
  EEPROM_write8(eepromMedDelay, medDelay_ms);
}

void setFastDelay() {
  lcd_clear();
  lcd_print("Fast Delay:");
  lcd_setCursor(4, 1);
  lcd_printf("%02u mS", fastDelay_ms);
  fastDelay_ms = setValue(fastDelay_ms, 0, 99, 4, 1);
  EEPROM_write8(eepromFastDelay, fastDelay_ms);
}

void setMainsFreq() {
  lcd_clear();
  lcd_print("Mains Frequency:");
  lcd_setCursor(4, 1);
  lcd_printf("%02u Hz", mainsFreq);
  mainsFreq = setValue(mainsFreq, 0, 99, 4, 1);
}

void lampOnISR() {
  lampInUse = 1;
  if (++brightness < maxBrightness) {
    setLampBrightness(&brightness);
  } else {
    IntDisable(lampInterrupt);
    lampInUse = 0;
  }
}

void lampOffISR() {
  lampInUse = 1;
  if (brightness && --brightness >= 0) {
    setLampBrightness(&brightness);
  } else {
    IntDisable(lampInterrupt);
    lampInUse = 0;
  }
}

void lampOn() {
  if (lampInUse || lampState) {
    return;
  }
  if (brightness == 0)
    brightness = minBrightness;
  lampInterrupt = OnTimer3(lampOnISR, INT_MILLISEC, 2);
  lampState = 1;
}

void lampFadeOn() {
  if (lampInUse || lampState) {
    return;
  }
  if (brightness == 0)
    brightness = minBrightness;
  unsigned short threshold1 = (unsigned short) ((maxBrightness - brightness) * 0.33);
  unsigned short threshold2 = (unsigned short) ((maxBrightness - brightness) * 0.66);
  while (brightness++ < maxBrightness && !digitalread(enterButton)) {
    setLampBrightness(&brightness);
    if (clockReady)
      printTime();
    if (brightness < threshold1) {
      Delayms(slowDelay_ms);
    } else if (brightness < threshold2) {
      Delayms(medDelay_ms);
    } else {
      Delayms(fastDelay_ms);
    }
  }
  lampState = 1;
}

void lampOff() {
  if (lampInUse) {
    return;
  }
  lampInterrupt = OnTimer3(lampOffISR, INT_MILLISEC, 2);
  lampState = 0;
}

void toggleLamp() {
  menuChange = 0;
  if (!lampState) {
    lampFadeOn();
  } else {
    lampOff();
    debounceButton(enterButton);
  }
  enterMainMenu();
}

void setBrightness(unsigned short *br, char instruction[16], char name[12],
                   unsigned short max, unsigned short min) {
  unsigned short timeoutCounter = 0;

  lcd_clear();
  lcd_print(instruction);
  lcd_setCursor(0, 1);
#ifdef BACKLIGHT_BRIGHTNESS
  if (brightnessIsBacklight)
    analogwrite(backlight, *br);
  else
    setLampBrightness(br);
#else
  setLampBrightness(br);
#endif
  lcd_setCursor(0, 1);
  lcd_write(DOWN_ARROW);
  lcd_setCursor(15, 1);
  lcd_write(UP_ARROW);
  lcd_setCursor(3, 1);
  lcd_printf("%s%04u", name, *br);
  lcd_setCursor(3, 1);
  /* debounceButton(upButton); */
  /* debounceButton(downButton); */
  unsigned char ret = 0;
  unsigned char ret2 = 0;
  while (!timeoutFlag && digitalread(enterButton)) {
    if (!ret && !ret2) {
      ret = pollButton(upButton, 1, 0);
    }
    if (ret == 1) {
      if (*br < max) {
        *br += 1;
      }
    } else if (ret == 2) {
      if (*br < max - 8) {
        *br += 8;
      } else {
        *br = max;
      }
    }

    if (!ret && !ret2) {
      ret2 = pollButton(downButton, 1, 0);
    }
    if (ret2 == 1) {
      if (*br > min) {
        *br -= 1;
      }
    } else if (ret2 == 2) {
      if (*br > min + 8) {
        *br -= 8;
      } else {
        *br = 0;
      }
    }
    if (ret || ret2) {
      timeoutCounter = 0; /* move with above? */
    }

#ifdef BACKLIGHT_BRIGHTNESS
    if (brightnessIsBacklight)
      analogwrite(backlight, *br);
    else
      setLampBrightness(br);
#else
    setLampBrightness(br);
#endif
    lcd_printf("%s%04u", name, *br);
    lcd_setCursor(3, 1);
    if (digitalread(downButton) && digitalread(upButton)) {
      ret = 0;
      ret2 = 0;
    }
    Delayms(50);
    if (++timeoutCounter == 600)
      timeoutFlag = 1;
  }

  if (!timeoutFlag)
    debounceButton(enterButton);

  timeoutFlag = 0;

  lampInUse = 0;
}

void adjustLamp() {
  if (!lampState) {
    lampOn();
  }
  setBrightness(&brightness, "Set Level", "Lamp: ", maxBrightness,
                minBrightness);
  if (brightness == 0)
    lampState = 0;
}

void setMinBrightness() {
  setBrightness(&minBrightness, "Set to glowing", "Min: ", maxduty, 0);
  setLampBrightness(&brightness);
  EEPROM_write16(eepromMinBrightness, minBrightness);
}

void setMaxBrightness() {
  setBrightness(&maxBrightness, "Set to full on", "Max: ", maxduty, 0);
  setLampBrightness(&brightness);
  EEPROM_write16(eepromMaxBrightness, maxBrightness);
}

/* --------------------------------------------------------------------
   Function pointer stuff
   ----------------------------------------------------------------------
*/

void nullFn(){};

void sunset() {
  lcd_clear();
  lampOn();
  lampInUse = 0; /* dare you to create race condition! */
  setTime(&fadeTime, "Sunset duration:");
  lcd_clear();
  lcd_print("Sun setting...");
  backlightOff();
  fade(fadeTime, 0); /* don't care about return value */
  lampOff();
  lampInUse = 0; /* ditto! */
  enterMainMenu();

  return;
}

void timeout() { menuTimeout = 1; }

void systemInfo() {
  lcd_clear();
  lcd_print("Firmware ");
  lcd_print(firmwareVersion);
  lcd_setCursor(0, 1);
  lcd_print("2e0byo/Pinguino");
  pollButton(enterButton, 0, 1);

  lcd_clear();
  lcd_print("customChars 0123");
  lcd_setCursor(12, 1);
  for (byte i = 0; i < 4; ++i) {
    lcd_write(i);
  }
  pollButton(enterButton, 0, 1);

  lcd_clear();
  lcd_printf("r. a. t. %u", realAlarmTime);
  lcd_setCursor(0, 1);
  lcd_printf("a. t. %u", alarmTime);
  pollButton(enterButton, 0, 1);
}

void factorySettings() {
  /* set everyting which is eepromed for first run */
  alarmEnabled = 0;
  ringOn = 1;
  writeEepromFlags;
  alarmTime = 0;
  EEPROM_write24(eepromAlarmTime, alarmTime);
  /* reset anything funny we might have done */
  /* guessMainsFreq(&mainsFreq); */
}

/* ================================================================== */
/* Menu functions */
/* ================================================================== */

void nextMenuEntry() {
  if (index < menuSize) {
    currentMenu++;
    index++;
  } else {
    for (unsigned char j = 0; j < menuSize; j++) {
      currentMenu--;
    }
    index = 0;
  }
}

void previousMenuEntry() {
  if (index > 0) {
    currentMenu--;
    index--;
  } else {
    index = menuSize;
    for (unsigned char j = 0; j < menuSize; j++) {
      currentMenu++;
    }
  }
}

void printPreviousMenuEntry() {
  previousMenuEntry();
  lcd_setCursor(0, 1);
  lcd_write(DOWN_ARROW);
  lcd_printf("%14s", currentMenu->name);
  lcd_write(UP_ARROW);
  nextMenuEntry();
}

static const unsigned char setupMenuNames[][15] = {
    "   Set Time   ", "Set Min Bright", "Set Max Bright", "Set Mains Freq",
    "Set Bkl Bright",  "Set Slow Delay", "Set Med Delay ", "Set Fast Delay",
    "Factory  Reset", " System  Info ",
};

menu setupMenu[] = {
    {setupMenuNames[0], setClock, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[1], setMinBrightness, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[2], setMaxBrightness, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[3], setMainsFreq, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
#ifdef BACKLIGHT_BRIGHTNESS
    {setupMenuNames[4], setBacklightBrightness, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
#endif
    {setupMenuNames[5], setSlowDelay, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[6], setMedDelay, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[7], setFastDelay, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[8], factorySettings, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {setupMenuNames[9], systemInfo, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry}

};

void enterSetupMenu() {
  index = 0;
  currentMenu = setupMenu;
#ifdef BACKLIGHT_BRIGHTNESS
  menuSize = 9;
#else
  menuSize = 8;
#endif
}

static const unsigned char alarmMenuNames[][15] = {
    "Enable/Disable", "Set Alarm Time", "Set Fade Time "};

menu alarmMenu[] = {
    {&alarmMenuNames[0], toggleAlarmEnabled, printAlarmStateLowerLine, timeout,
     enterMainMenu, nextMenuEntry,
     previousMenuEntry}, /* do we need to move lines for print alarm state? */
    {&alarmMenuNames[1], setAlarm, printPreviousMenuEntry, timeout,
     enterMainMenu, nextMenuEntry, previousMenuEntry},
    {&alarmMenuNames[2], setFadeOnMinutes, nullFn, printPreviousMenuEntry,
     enterMainMenu, nextMenuEntry, previousMenuEntry}};

void enterAlarmMenu() {
  index = 0;
  currentMenu = alarmMenu;
  menuSize = 2;
}

static const unsigned char emptystr[] = "";

static const unsigned char mainMenuNames[][15] = {
    " Start Sunset ", " Setup Alarm ", " System Setup ", "      Nap     "};

menu mainMenu[] = {{&emptystr, toggleBacklight, homeScreen, backlightOff,
                    editSnoozeOrToggleLamp, adjustLamp, adjustLamp},
                   {&mainMenuNames[0], sunset, printPreviousMenuEntry, timeout,
                    enterMainMenu, nextMenuEntry, previousMenuEntry},
                   {&mainMenuNames[1], enterAlarmMenu, printPreviousMenuEntry,
                    timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
                   {&mainMenuNames[2], enterSetupMenu, printPreviousMenuEntry,
                    timeout, enterMainMenu, nextMenuEntry, previousMenuEntry},
                   {&mainMenuNames[3], nap, printPreviousMenuEntry, timeout,
                    enterMainMenu, nextMenuEntry, previousMenuEntry},
                   {&alarmMenuNames[1], setAlarm, printPreviousMenuEntry,
                    timeout, enterMainMenu, nextMenuEntry, previousMenuEntry}};

void enterMainMenu() {
  index = 0;
  currentMenu = mainMenu;
  menuSize = 5;
}

void homeScreen() {
  lcd_home();
  if (clockReady) {
    printAlarmState();
    printTime();
    if (savedAlarmTime) {
      printSnooze();
    }
  }
}

void menuLoop() {

  /*
     while not enter button:
     - if up, move up, print, then execute displayFunc
     - if down, ditto
     Then:
     - if long press: run exitCurrentmenu
     - else run enterFunc

     The menuLoop doesn't die unless displayMenu is toggled from
     elsewhere.  Currently this can only happen if an alarm elapses
     (in which case we return to the main loop for a bit).

  */

  unsigned char countFlag = 0;
  menuChange = 1;
  while (!alarmFlag) {
    if (menuChange) {
      lcd_home();
      if (&(currentMenu->name)[0] != emptystr) {
        lcd_printf("-%14s-", currentMenu->name);
      }
      currentMenu->displayFunc();
      debounceButton(enterButton);
    }
    menuChange = 1;

    if (countFlag == 2) {
      debounceButton(enterButton);
      countFlag = 0;
    }

    unsigned char ret;

    while (!alarmFlag && !menuTimeout && digitalread(enterButton)) {

      currentMenu
          ->displayFunc(); /* mostly does nothing, homeScreen called here */

      if (ret == 2) {
        debounceButton(upButton);
        debounceButton(downButton);
      }

      idleCount++;

      if (idleCount > 600) {
        currentMenu->timeoutFunc();
        idleCount = 0;
      }

      if (!backlightStatus) {
        if (!digitalread(upButton) || !digitalread(downButton)) {
          backlightOn();
          idleCount = 0;
          while (!digitalread(upButton) || !digitalread(downButton)) {
            Delayms(100);
          }
          Delayms(100); /* debounce */
          continue;
        }
      }

      ret = pollButton(downButton, 1, 0);
      if (ret)
        idleCount = 0;

      if (ret == 1) {
        previousMenuEntry();
      } else if (ret == 2) {
        currentMenu->longDownFunction();
      }

      if (!ret) {
        ret = pollButton(upButton, 1, 0);
        if (ret)
          idleCount = 0;

        if (ret == 1) {
          nextMenuEntry();
        } else if (ret == 2) {
          currentMenu->longUpFunction();
        }
      }

      if (ret) {
        lcd_clear();
        lcd_printf("-%14s-", currentMenu->name);
        currentMenu->displayFunc();
      } else {
        Delayms(100);
      }
    }

    if (menuTimeout) {
      menuTimeout = 0;
      enterMainMenu();
      continue;
    }

    countFlag = pollButton(enterButton, 1, 0);
    if (countFlag == 2) {
      currentMenu->exitCurrentMenu();
    } else if (countFlag == 1) {
      currentMenu->enterFunc();
    } else {
      Delayms(50); /* Main Loop Delay */
    }
  }
}

void snooze() {
  /* Setup a snooze alarm.  This stores the current alarm and fade on
     value, and then replaces them with a new alarm. */

  lcd_clear();
  lcd_print("Snooze time:");
  lcd_setCursor(3, 1);
  lcd_printf("%02u", snoozeMinutes);
  lcd_print(" Minutes");
  while (!digitalread(enterButton)) {
    Delayms(100);
  }

  snoozeMinutes = setValue(snoozeMinutes, 3, 59, 3, 1);

  if (toggleValue(0, "Cancel Snooze", "Continue")) {
    if (savedAlarmTime) {
      alarmTime = savedAlarmTime;
      savedAlarmTime = 0;
      fadeOnMinutes = savedFadeOnMinutes;
      adjustAlarm();
    }
    return;
  }

  if (!savedAlarmTime) {
    savedAlarmTime = alarmTime;
  }
  alarmTime = time + snoozeMinutes * 60;
  if (alarmTime > day) {
    alarmTime = snoozeMinutes * 60 - time;
  }
  savedFadeOnMinutes = fadeOnMinutes;
  if (snoozeMinutes > 5) {
    fadeOnMinutes = 5;
  } else {
    fadeOnMinutes = 2;
  }

  adjustAlarm();
  backlightOff();
}

void editSnoozeOrToggleLamp() {
  if (savedAlarmTime) {
    snooze();
  } else {
    toggleLamp();
  }
}

void nap() {
  snooze();
  enterMainMenu();
}


void customChars() {
  lcd_createChar(0, downarrow);
  lcd_createChar(1, uparrow);

  lcd_createChar(2, downarrow); /* testing copies */
  lcd_createChar(3, uparrow);
  lcd_home();
}

void setup() {
#ifdef INVERSE_LAMP_LOGIC /* need to init early to turn lamp off */
  initLamp();
#endif
#include "buttons_hal.c"

  analogwrite_init();
  pinmode(upButton, INPUT);
  pinmode(downButton, INPUT);
  pinmode(enterButton, INPUT);

  lcd_pins(lcdRS, lcdEnable, lcdd0, lcdd1, lcdd2, lcdd3, lcdd4, lcdd5, lcdd6,
           lcdd7);

  lcd_begin(16, 2);

  backlightInit();
  backlightOn();
  lcd_clear();

  customChars();

  readEepromFlags();

  time = 0;
  setClock();
  minBrightness = EEPROM_read16(eepromMinBrightness);
  if (minBrightness > maxduty) {
    minBrightness = 0;
  }
  maxBrightness = EEPROM_read16(eepromMaxBrightness);
  if (maxBrightness > maxduty) {
    maxBrightness = maxduty;
  }
  fadeOnMinutes = EEPROM_read8(eepromFadeOnMinutes);
  if (fadeOnMinutes > 59) {
    fadeOnMinutes = 30;
  }

  alarmTime = EEPROM_read24(eepromAlarmTime);
  if (alarmTime > day) {
    alarmTime = 0;
    alarmEnabled = 0;
  }
  adjustAlarm();
  /* guessMainsFreq(&mainsFreq); */
  slowDelay_ms = EEPROM_read8(eepromSlowDelay);
  medDelay_ms = EEPROM_read8(eepromMedDelay);
  fastDelay_ms = EEPROM_read8(eepromFastDelay);
#ifdef TRAILING_EDGE_DIMMER
  OnChangePin0(tick, INT_RISING_EDGE);
  OnChangePin1(ticktock, INT_FALLING_EDGE);
#else
  OnChangePin0(second, INT_RISING_EDGE);
#endif
  initLamp();
}

void printInterruptStatus() {
  lcd_clear();
  lcd_printf("%u:%u", intUsed[INT_TMR0], intCount[INT_TMR0]);
  lcd_setCursor(0, 1);
  lcd_printf("%u", T0CON);
  pollButton(enterButton, 0, 1);
}

void loop() {
  enterMainMenu();
  menuLoop(); /* we only leave when alarm elapses */

  /* ring */
  lcd_clear();
  lcd_home();
  lcd_print("Ring! Ring Ring!");
  unsigned char status = fadeOn();
  backlightOn();
  if (!status) {
    status = ring();
  }
  if (savedAlarmTime) {
    alarmTime = savedAlarmTime;
    savedAlarmTime = 0;
    fadeOnMinutes = savedFadeOnMinutes;
    adjustAlarm();
  }
  if (status == 2) {
    snooze();
  }

  backlightOff();

  /* end */
  lampOff();
  alarmFlag = 0;
  /* in case corrupted */
  lcd_begin(16, 2);
  lcd_clear();
  customChars();
  lcd_home();
}
