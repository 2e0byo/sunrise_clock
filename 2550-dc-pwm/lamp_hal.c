/* HAL since multiple methods of controlling the lamp are used */

#define lamp 12
#define INVERSE_LAMP_LOGIC
#define maxduty 1023

unsigned char dimmerInterrupt = 0;


/**
 * @brief Set Lamp Brightness.
 * @details Wrapper function for different hardware.
 * @param[inout] lampBrightness Brightness.
 */
void setLampBrightness(unsigned short *lampBrightness) {
#ifdef INVERSE_LAMP_LOGIC
  analogwrite(lamp, 1023 - *lampBrightness);
#else
  analogwrite(lamp, *lampBrightness);
#endif
}

/**
 * @brief Setup Lamp
 * @details Turn off and enable dimmer interrupt.
 */
void initLamp(){
  pinmode(lamp, OUTPUT);
  digitalwrite(lamp, HIGH);
  IntEnable(dimmerInterrupt);
}
