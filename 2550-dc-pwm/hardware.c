#include "hardware.h"
#include <delayms.c>
#include <digitalr.c>


/**
 * @brief Poll a Button
 * @details The main button polling function
 * @param[in] button Button to poll.
 * @param[in] waitLongPress Whether to wait for a long press.
 * @param[in] wait Whether to block until pressed.
 * @return Returns 0 if the button is unpressed, 1 for short and 2 for long press.
 */
unsigned char pollButton(unsigned char button, char waitLongPress, char wait) {
  while (wait && digitalread(button)) {
    Delayms(100);
  }
  unsigned char count = 0;
  if (!digitalread(button)) {
    while (1) {
      while (!digitalread(button)) {
        count++;
        if (waitLongPress && count > longPress) {
          return 2;
        }
        Delayms(100);
      }
      Delayms(100); /* debounce */
      if (digitalread(button))  /* wasn't just a flick */
        return 1;
    }
  }
  return 0;
}

/**
 * @brief Debounce a button.
 * @details Block whilst the button is pressed, then pause until it has settled.
 * @param[in] button The button to debounce.
 */
void debounceButton(unsigned char button) {
  while (!digitalread(button)) {
    Delayms(50);
  }
  Delayms(100); /* debounce */
}
