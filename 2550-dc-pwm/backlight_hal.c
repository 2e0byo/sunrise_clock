#include "hardware.h"
/* HAL since multiple methods of controlling the backlight are used */

bit backlightStatus;
unsigned short backlightBrightness;

char brightnessIsBacklight = 0;

static const u8 eepromBacklightBrightness = 11;

void backlightOff(){
  digitalwrite(backlight, LOW);
  backlightStatus = 0;
}

void backlightOn(){
  if (backlightStatus) return;	/* called repeatedly by timeout loop */
  digitalwrite(backlight, HIGH);
  backlightStatus = 1;
}


void backlightInit(){
  pinmode(backlight, OUTPUT);
}
