#define ANALOGWRITE
#define INTDISABLE
#define INTENABLE
#define LCDCLEAR
#define LCDCURSOR
#define LCDHOME
#define LCDNOCURSOR
#define LCDPRINT
#define LCDPRINTF
#define LCDSETCURSOR
#define TMR0INT
#define TMR1INT
#define TMR3INT
#define INT0INT
#define INT1INT




#include "analog.c"
#include <audio.c>
#include "eeprom.c"
#include <delayms.c>
#include <digitalp.c>
#include <digitalr.c>
#include <digitalw.c>
#include <interrupt.c>
#include "lcdlib.c"
#include <macro.h>
#include <typedef.h>
