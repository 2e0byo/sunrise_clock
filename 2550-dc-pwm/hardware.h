#ifndef HARDWARE
#define HARDWARE
#define backlight 6
#define downButton 16
#define enterButton 17
#define longPress 5
#define upButton 15

/* LCD pins */
#define lcdRS 8
#define lcdEnable 1
#define lcdd0 0
#define lcdd1 0
#define lcdd2 0
#define lcdd3 0
#define lcdd4 2
#define lcdd5 3
#define lcdd6 4
#define lcdd7 5
/* lcd chars */

#define DOWN_ARROW 0
#define UP_ARROW 1

#define LINEOUT PWM2
#endif
