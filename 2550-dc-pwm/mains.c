#include "mains.h"
#include <delayus.c>
#include <digitalr.c>
#include <digitalp.c>

void guessMainsFreq(unsigned char *mainsFreq) {
  /* Poll the mains pin to see if we can work out the period */
  static bit tmpBit;
  static unsigned char pps;

#ifdef TRAILING_EDGE_DIMMER
  pinmode(1, INPUT);
#endif
  pinmode(0, INPUT);
  pps = 0;

  /* Poll until it toggles */
  tmpBit = digitalread(0);
  while (digitalread(0) == tmpBit) {
    Delayus(1);
  }

  while (digitalread(0) != tmpBit) {
    pps++;
    Delayus(500);
  }

  *mainsFreq = (pps < 19) ? 60 : 50;
}
