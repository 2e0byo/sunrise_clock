/*-----------------------------------------------------
  Author:  --<>
  Date: 2019-09-20
  Description:

  -----------------------------------------------------*/
#define lamp 12
#define relay 24
#define backlight 9
#define upButton 21
#define downButton 22
#define enterButton 23
#define LINEOUT PWM2

//u8 rate = 752;		/* Seconds lost/gained every day  */
//u8 leapMinutes;	/* Every n minutes add or remove a second */
//u8 dailyRemainder; 		/* Seconds remaining to add/subtract at end of day */
int displayClock = 0;
int stopClock = 0;
int stopRing = 0;
int backlightInterrupt;
int clockInterrupt;
u8 clockMajorFlag;

unsigned int maxBrightness = 1023;

u8 s = 0;
u8 m = 0;
u8 h = 0;

u8 alarmH = 0;
u8 alarmM = 0;
u8 alarmS = 0;			/* ignored */
u8 realAlarmM = 0;
u8 realAlarmH = 0;
u8 alarmEnabled = true;
u8 alarmFlag = false;
u8 fadeOnMinutes = 30;
u8 ringOn = true;

// Modified from the shipped example
// Melody structure
typedef struct
{
    int freq;
    int rest;
} Note;


Note melody[] = {
    {NOTE_C4, 4},
    {NOTE_G3, 8},
    {NOTE_G3, 8},
    {NOTE_A3, 4},
    {NOTE_G3, 4},
    {0,       4},
    {NOTE_B3, 4},
    {NOTE_C4, 4}};

void ring()
{
  if (!ringOn){
    stopRing = 1;

    while (digitalRead(enterButton) && digitalRead(upButton) && digitalRead(downButton)) {
      delay(100);
    }
    
    while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
      delay(100);
    }
  }
  
  while (!stopRing) {
    int thisNote;	
    int pauseBetweenNotes;
    int noteDuration;
    // iterate over the notes of the melody:
    for (thisNote = 0; thisNote < 8; thisNote++)
    {
        // to calculate the note duration, take 1/2 a second 
        // divided by the note rest.
        noteDuration = 500 / melody[thisNote].rest;

        Audio.tone(LINEOUT, melody[thisNote].freq, noteDuration);

        // to distinguish the notes, set a minimum time between them.
        // the note's duration + 30% seems to work well:
        pauseBetweenNotes = noteDuration +  noteDuration / 30;
        delay(pauseBetweenNotes);
	if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	  stopRing = 1;
	  break;
	}
    }
    
    // otherwise, stop the tone playing:
    Audio.noTone(LINEOUT);

    // and wait for 1 second, polling.
    for (thisNote = 0; thisNote < 100; thisNote++) {
      delay(100);
      if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	stopRing = 1;
	break;
      }
    }

  }
  while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
    delay(100);
  }
  
}

// void calculateRate() {
//   leapMinutes = 1440 / rate;
//   dailyRemainder = rate - leapMinutes * 24; /* leftover to be applied at the end of the day */
//   lcd.printf("LeapMinutes %i LeapRemainder %i", leapMinutes, dailyRemainder);
//   delay(1000);
// }

void tick(){			// function called every second
  if (!stopClock) {
    s++;
    if (s > 59)
      {
	s = 0;
	m++;
	if (m > 59)
	  {
	    m = 0;
	    h++;
	    if (h > 23)
	      {
		h = 0;
	      }
	  }
      }
    if (m == realAlarmM && h == realAlarmH && alarmEnabled) { // don't check for second in case we adjust
      alarmFlag = true;
    }
    if (displayClock) {
      lcd.setCursor(4, 1);
      lcd.printf("%02u:%02u:%02u", h, m, s);
    }
  }
}
void printAlarmState(){
  if (alarmEnabled) {
    lcd.print("Alarm On: ");
  }
  else {
    lcd.print("Alarm Off:");
  }
  lcd.printf(" %02u:%02u", alarmH, alarmM);
}



void lampOn(){
  analogWrite(lamp, 0);
  digitalWrite(relay, HIGH);
}

void printJustified(u8 value, u8 max){
  if (max < 10){
    lcd.print(value);
  }
  else if (max < 100){
    lcd.printf("%02u",value);
  }
  else if (max < 1000){
    lcd.printf("%03u",value);
  }
}

u8 toggleValue(u8 value, char name[16]){
  lcd.clear();
  lcd.print(name);
  lcd.setCursor(4,1);
  if (value) {
    lcd.print("True");
  }
  else {
    lcd.print("False ");
  }
  lcd.setCursor(4,1);
  
  while (digitalRead(enterButton)){
    if (!digitalRead(upButton) || !digitalRead(downButton)){
      if (value){
	value = 0;
	lcd.print("False");
      }
      else {
	value = 1;
	lcd.print("True ");
      }
      lcd.setCursor(4, 1);
      while (!digitalRead(upButton)){
	delay(100); 		/* wait for button release */
      }
    }
    delay(100);
  }
  while (!digitalRead(enterButton)){
    delay(100);
  }
  return (value);
}

u8 setValue(u8 value, u8 min, u8 max, u8 col){
  while (digitalRead(enterButton)){
    if (!digitalRead(upButton)){
      value++;
      if (value >= max){
	value = min;
      }
      printJustified(value, max);
      lcd.setCursor(col, 1);
      while (!digitalRead(upButton)){
	delay(100); 		/* wait for button release */
      }
    }
    else if (!digitalRead(downButton)){
      if (value > min){
	value--;
      }
      else {
	value = max;
      }
      printJustified(value, max);
      lcd.setCursor(col, 1);
      while (!digitalRead(downButton)) {
	delay(100);
      }
    }
    delay(100);
  }
  while (!digitalRead(enterButton)){
    delay(100);
  }
  return (value);
}

void setTime(u8 *hr, u8 *min, u8 *sec, char name[16]){
  /* Set a time with buttons */
  displayClock = false;
  lcd.clear();
  lcd.print(name);
  lcd.setCursor(4,1);
  u8 local_hour = *hr;
  u8 local_min = *min;
  u8 local_sec = *sec;
  lcd.printf("%02u:%02u:%02u", local_hour, local_min, local_sec);
  lcd.setCursor(4,1);
  lcd.cursor();
  local_hour = setValue(local_hour, 0, 23, 4);
  lcd.setCursor(7, 1);
  local_min = setValue(local_min, 0, 59, 7);
  lcd.setCursor(10, 1);
  local_sec = setValue(local_sec, 0, 59, 10);
  lcd.noCursor();
  *hr = local_hour;
  *min = local_min;
  *sec = local_sec;
}

void adjustAlarm() {
  if (alarmM >= fadeOnMinutes) {
    realAlarmM = alarmM - fadeOnMinutes;
    realAlarmH = alarmH;
  }
  else {
    realAlarmM = 60 + alarmM - fadeOnMinutes;

    if (alarmH == 0) {
      realAlarmH = 23;
    }
    else {
      realAlarmH = alarmH - 1;
    }
  }
}

int fadeOn(u8 delayMins) {
  alarmFlag = false;
  unsigned int brightness;
  unsigned long del;
  unsigned long frac = delayMins * 600 / maxBrightness; // integer division
  for (brightness = 0; brightness <= maxBrightness; brightness++ ){
    analogWrite(lamp, brightness);

    for (del =0; del <= frac; del++ ){
      delay(100);
      
      if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	  delay(100);
	}
	return 1;
      }
      
    }
    // delay(delayMins * 60000 / 1024);
  }
  return 0;
}

void backlightOff(){
  digitalWrite(backlight, LOW);
}

void setup() {
  // put your setup code here, to run once:
  Audio.init(TAPEQUALITY);
  Audio.staccato();
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
  pinMode(enterButton, INPUT);
  pinMode(relay, OUTPUT);
  lcd.pins(0,1,0,0,0,0,2,3,4,5); // RS, E, D4 ~ D8	
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  pinMode(backlight, OUTPUT);
  pinMode(lamp,OUTPUT);
  //PWM.setFrequency(10000);
  OnTimer0(tick, INT_MILLISEC, 993);
  digitalWrite(backlight, HIGH);
  lcd.clear();
  stopClock = true;
  setTime(&h, &m, &s, "Time:");
  setTime(&alarmH,&alarmM,&alarmS,"Alarm:");
  adjustAlarm();
  ringOn = toggleValue(ringOn, "Ring enabled:");
  stopClock = false;
  lcd.clear();
  lcd.home();
  printAlarmState();
  displayClock = true;
  backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
}

void safePrint(char msg[16]){
  displayClock = false;
  lcd.print(msg);
  displayClock = true;
}

void homeScreen(){
  displayClock = false;
  lcd.home();
  printAlarmState();
  displayClock = true;
  backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
}

int jumpFlag = false;

void loop() {
  // put your main code here, to run repeatedly:
  //lampOn();
  //fadeOn(2);
  /* menu entry? */
  if (!digitalRead(backlight)){
    if  (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
      Int.detach(backlightInterrupt);
      digitalWrite(backlight, HIGH);
      backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
      while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
  	delay(100);
      }
      jumpFlag = true;
    }
  }

  if (!jumpFlag){
  
  if (!digitalRead(enterButton)){
    /* Int.detach(backlightInterrupt); */
    /* digitalWrite(backlight, HIGH); */
    if (alarmEnabled){
      alarmEnabled = false;
    }
    else {
      alarmEnabled = true;
      ringOn = toggleValue(ringOn, "Ring Enabled:");
    }
    homeScreen();
    while (!digitalRead(enterButton)) {
      delay(100);
    }
  }
  
  if (!digitalRead(downButton)){
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    while (!digitalRead(downButton)) {
      delay(100);
    }
    setTime(&h, &m, &s, "Time:");
    homeScreen();
  }
  if (!digitalRead(upButton)){
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    while (!digitalRead(upButton)) {
      delay(100);
    }
    setTime(&alarmH, &alarmM, &alarmS, "Alarm:");
    adjustAlarm();
    homeScreen();
  }
  
  /* check for alarm */
  if (alarmFlag) {
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    displayClock = false;
    lcd.clear();
    lcd.home();
    lcd.print("Ring! Ring Ring!");
    displayClock = true;
    digitalWrite(relay, HIGH);
    if (!fadeOn(fadeOnMinutes)){
      ring();
    }
    alarmFlag = false;
    digitalWrite(relay, LOW);
    displayClock = false;
    lcd.begin(16, 2);
    lcd.clear();
    lcd.home();
    lcd.setCursor(0,0);
    printAlarmState();
    displayClock = true;
    backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
  }
  delay(100);
  }
  else {
    jumpFlag = false;
  }
}

