/*-----------------------------------------------------
  Author:  --<>
  Date: 2019-09-20
  Description:

  -----------------------------------------------------*/
#define lamp 12
// nc
#define backlight 6
#define upButton 16
#define downButton 15
#define enterButton 17
#define LINEOUT PWM2
// multiples of 100ms
#define longPress 5



//unsigned int rate = 752;		/* Seconds lost/gained every day  */
//unsigned int leapMinutes;	/* Every n minutes add or remove a second */
//unsigned int dailyRemainder; 		/* Seconds remaining to add/subtract at end of day */
int displayClock = 0;
int stopClock = 0;
int stopRing = 0;
int backlightInterrupt;
int clockInterrupt;
unsigned int clockMajorFlag;

unsigned int maxBrightness = 1023;
unsigned int minBrightness = 0;
unsigned int tickPeriod = 993;


unsigned int s = 0;
unsigned int m = 0;
unsigned int h = 0;

unsigned int alarmH = 0;
unsigned int alarmM = 0;
unsigned int alarmS = 0;			/* ignored */
unsigned int realAlarmM = 0;
unsigned int realAlarmH = 0;
unsigned int alarmEnabled = true;
unsigned int alarmFlag = false;
unsigned int fadeOnMinutes = 30;
unsigned int ringOn = true;

// Modified from the shipped example
// Melody structure
typedef struct
{
    int freq;
    int rest;
} Note;


Note melody[] = {
    {NOTE_C4, 4},
    {NOTE_G3, 8},
    {NOTE_G3, 8},
    {NOTE_A3, 4},
    {NOTE_G3, 4},
    {0,       4},
    {NOTE_B3, 4},
    {NOTE_C4, 4}};

void ring()
{
  if (!ringOn){
    stopRing = 1;

    while (digitalRead(enterButton) && digitalRead(upButton) && digitalRead(downButton)) {
      delay(100);
    }
    
    while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
      delay(100);
    }
  }
  
  while (!stopRing) {
    int thisNote;	
    int pauseBetweenNotes;
    int noteDuration;
    // iterate over the notes of the melody:
    for (thisNote = 0; thisNote < 8; thisNote++)
    {
        // to calculate the note duration, take 1/2 a second 
        // divided by the note rest.
        noteDuration = 500 / melody[thisNote].rest;

        Audio.tone(LINEOUT, melody[thisNote].freq, noteDuration);

        // to distinguish the notes, set a minimum time between them.
        // the note's duration + 30% seems to work well:
        pauseBetweenNotes = noteDuration +  noteDuration / 30;
        delay(pauseBetweenNotes);
	if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	  stopRing = 1;
	  break;
	}
    }
    
    // otherwise, stop the tone playing:
    Audio.noTone(LINEOUT);

    // and wait for 1 second, polling.
    for (thisNote = 0; thisNote < 100; thisNote++) {
      delay(100);
      if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	stopRing = 1;
	break;
      }
    }

  }
  while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
    delay(100);
  }
  
}

// void calculateRate() {
//   leapMinutes = 1440 / rate;
//   dailyRemainder = rate - leapMinutes * 24; /* leftover to be applied at the end of the day */
//   lcd.printf("LeapMinutes %i LeapRemainder %i", leapMinutes, dailyRemainder);
//   delay(1000);
// }

void tick(){			// function called every second
  if (!stopClock) {
    s++;
    if (s > 59)
      {
	s = 0;
	m++;
	if (m > 59)
	  {
	    m = 0;
	    h++;
	    if (h > 23)
	      {
		h = 0;
	      }
	  }
      }
    if (m == realAlarmM && h == realAlarmH && alarmEnabled) { // don't check for second in case we adjust
      alarmFlag = true;
    }
    if (displayClock) {
      lcd.setCursor(4, 1);
      lcd.printf("%02u:%02u:%02u", h, m, s);
    }
  }
}
void printAlarmState(){
  if (alarmEnabled) {
    lcd.print("Alarm On: ");
  }
  else {
    lcd.print("Alarm Off:");
  }
  lcd.printf(" %02u:%02u", alarmH, alarmM);
}


int fadeOn(unsigned int delayMins) {
  alarmFlag = false;
  unsigned int brightness;
  unsigned long del;
  unsigned long frac = delayMins * 600 / (maxBrightness - minBrightness); // integer division
  for (brightness = minBrightness; brightness <= maxBrightness; brightness++ ){
    analogWrite(lamp, brightness);

    for (del =0; del <= frac; del++ ){
      delay(100);
      
      if (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
	  delay(100);
	}
	return 1;
      }
      
    }
    // delay(delayMins * 60000 / 1024);
  }
  return 0;
}

void backlightOff(){
  digitalWrite(backlight, LOW);
}

void printJustified(unsigned int value, unsigned int max){
  if (max < 10){
    lcd.print(value);
  }
  else if (max < 100){
    lcd.printf("%02u",value);
  }
  else if (max < 1000){
    lcd.printf("%03u",value);
  }
}

unsigned int toggleValue(unsigned int value, char name[16]){
  lcd.clear();
  lcd.print(name);
  lcd.setCursor(4,1);
  if (value) {
    lcd.print("True");
  }
  else {
    lcd.print("False ");
  }
  lcd.setCursor(4,1);
  
  while (digitalRead(enterButton)){
    if (!digitalRead(upButton) || !digitalRead(downButton)){
      if (value){
	value = 0;
	lcd.print("False");
      }
      else {
	value = 1;
	lcd.print("True ");
      }
      lcd.setCursor(4, 1);
      while (!digitalRead(upButton)){
	delay(100); 		/* wait for button release */
      }
    }
    delay(100);
  }
  while (!digitalRead(enterButton)){
    delay(100);
  }
  return (value);
}

unsigned int setValue(unsigned int value, unsigned int min, unsigned int max, unsigned int col) {
  while (digitalRead(enterButton)) {
  unsigned int count = 0;
  unsigned int contFlag = 0;
    if (!digitalRead(upButton)) {
      while (!digitalRead(upButton)) {
  	delay(100);
        count++;
  	if (count > longPress) {
  	  count = 0;		// for now we advance at same rate as entry
  	  if (value < (max - 2)) { 
  	    value += 2;
  	  }
  	  else if (value < max) {
  	    value = max; 
  	  }
  	  else {
  	    value = min;
  	  }
  	  printJustified(value, max);
  	  lcd.setCursor(col, 1);
  	  contFlag = 1;
  	}
      }
      if (!contFlag) {
  	if (value < max) {
  	  value++;
  	}
  	else {
  	  value = min;
  	}
  	printJustified(value, max);
  	lcd.setCursor(col, 1);
      }
    }
    
    else if (!digitalRead(downButton)) {
      while (!digitalRead(downButton)) {
	delay(100);
        count++;
	if (count > longPress) {
	  count = 0;		// for now we advance at same rate as entry

	  if (value > (min + 2)) { 
	    value -= 2;
	  }
	  else if (value > min) {
	    value = min; 
	  }
	  else {
	    value = max;
	  }
	  
	  printJustified(value, max);
	  lcd.setCursor(col, 1);
	  contFlag = 1;
	}
      }
      if (!contFlag) {
	if (value > min) {
	  value--;
	}
	else {
	  value = max;
	}
	printJustified(value, max);
	lcd.setCursor(col, 1);
      }
    }
  }
 
  while (!digitalRead(enterButton)){
    delay(100);
  }
  
  return (value);
}

void setTime(unsigned int *hr, unsigned int *min, unsigned int *sec, char name[16]){
  /* Set a time with buttons */
  displayClock = false;
  lcd.clear();
  lcd.print(name);
  lcd.setCursor(4,1);
  unsigned int local_hour = *hr;
  unsigned int local_min = *min;
  unsigned int local_sec = *sec;
  lcd.printf("%02u:%02u:%02u", local_hour, local_min, local_sec);
  lcd.setCursor(4,1);
  lcd.cursor();
  local_hour = setValue(local_hour, 0, 23, 4);
  lcd.setCursor(7, 1);
  local_min = setValue(local_min, 0, 59, 7);
  lcd.setCursor(10, 1);
  local_sec = setValue(local_sec, 0, 59, 10);
  lcd.noCursor();
  *hr = local_hour;
  *min = local_min;
  *sec = local_sec;
}


unsigned int setBrightness(unsigned int br, char instruction[16], char name[12]) {
  displayClock = false;
  lcd.clear();
  lcd.print(instruction);
  lcd.setCursor(0,1);
  analogWrite(lamp, br);
  lcd.printf("%s%04u", name, br);
  lcd.setCursor(0,1);
  while (digitalRead(enterButton)) {
    unsigned int count = 0;
    unsigned int contFlag = 0;
    if (!digitalRead(upButton)) {
      while (!digitalRead(upButton)) {
	delay(100);
        count++;
	if (count > longPress) {
	  count = 0;		// for now we advance at same rate as entry
	  if (br < 1015) { 
	    br += 8;
	  }
	  else {
	    br = 1023; // strange decision!
	  }
	  analogWrite(lamp, br);
	  lcd.printf("%s%04u", name, br);
	  lcd.setCursor(0,1);
	  contFlag = 1;
	}
      }
      if (!contFlag) {
	if (br < 1023) {
	  br++;
	  analogWrite(lamp, br);
	}
	lcd.printf("%s%04u", name, br);
	lcd.setCursor(0,1);
      }
    }
    
    else if (!digitalRead(downButton)) {
      while (!digitalRead(downButton)) {
	delay(100);
        count++;
	if (count > longPress) {
	  count = 0;		// for now we advance at same rate as entry
	  if (br > 7) {
	    br -= 8;
	  }
	  else {
	    br = 0;
	  }
	  analogWrite(lamp, br);
	  lcd.printf("%s%04u", name, br);
	  lcd.setCursor(0,1);
	  contFlag = 1;
	}
      }
      if (!contFlag) {
	if (br > 0) {
	  br--;
	}
	analogWrite(lamp, br);
	lcd.printf("%s%04u", name, br);
	lcd.setCursor(0,1);
      }
    }

    delay(100);			// if nothing
  }
  while (!digitalRead(enterButton)) {
    delay(100);
  }
  
  digitalWrite(lamp, LOW);
  return br;
}

void setMinBrightness() {
  minBrightness = setBrightness(minBrightness, "Set to glowing", "Min Bright: ");
}

void setMaxBrightness() {
  maxBrightness = setBrightness(maxBrightness, "Set to full on", "Max Bright: ");
}

void adjustAlarm() {
  if (alarmM >= fadeOnMinutes) {
    realAlarmM = alarmM - fadeOnMinutes;
    realAlarmH = alarmH;
  }
  else {
    realAlarmM = 60 + alarmM - fadeOnMinutes;

    if (alarmH == 0) {
      realAlarmH = 23;
    }
    else {
      realAlarmH = alarmH - 1;
    }
  }
}

// typedef void (*voidFn)(void);

//void (*voidFn)(void);
// void* voidFn = setTime;

// typedef struct
// {
//   char screenName[16];
//   void* enterFn ;

// } screen;

// // setupScreens[0].enterFn = setTime;
// int sum(int a, int b);
// int subtract(int a, int b);
// int mul(int a, int b);
// int div(int a, int b);
// 
// int (*p[4]) (int x, int y);
//   p[0] = sum; /* address of sum() */
//   p[1] = subtract; /* address of subtract() */
//   p[2] = mul; /* address of mul() */
//   p[3] = div; /* address of div() */
// 
// void (*setupScreens[4]) (void);
//setupScreens[0] = setTime;

// void setupMenu() {
//   lcd.clear();
//   lcd.print(setupScreens[0].screenName);
//   setupScreens[0].enterFunction();
// }

void homeScreen(){
  displayClock = false;
  lcd.home();
  printAlarmState();
  displayClock = true;
  backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
}


void setup() {
  // put your setup code here, to run once:
  Audio.init(TAPEQUALITY);
  Audio.staccato();
  analogwrite_init();
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
  pinMode(enterButton, INPUT);
  lcd.pins(0,1,0,0,0,0,2,3,4,5); // RS, E, D4 ~ D8	
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  pinMode(backlight, OUTPUT);
  pinMode(lamp,OUTPUT);
  //PWM.setFrequency(10000);
  OnTimer0(tick, INT_MILLISEC, tickPeriod);
  digitalWrite(backlight, HIGH);
  lcd.clear();
  stopClock = true;
  setTime(&h, &m, &s, "Time:");
  stopClock = false;		// clock starts on next interrupt
  setTime(&alarmH,&alarmM,&alarmS,"Alarm:");
  adjustAlarm();
  ringOn = toggleValue(ringOn, "Ring enabled:");
  setMinBrightness();
  setMaxBrightness();
  lcd.clear();
  lcd.home();
  printAlarmState();
  displayClock = true;
  backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
}

int jumpFlag = false;

void loop() {
  // put your main code here, to run repeatedly:
  //lampOn();
  //fadeOn(2);
  /* menu entry? */
  if (!digitalRead(backlight)){
    if  (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
      Int.detach(backlightInterrupt);
      digitalWrite(backlight, HIGH);
      backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
      while (!digitalRead(enterButton) || !digitalRead(upButton) || !digitalRead(downButton)) {
  	delay(100);
      }
      jumpFlag = true;
    }
  }

  if (!jumpFlag){
  
  if (!digitalRead(enterButton)){
    /* Int.detach(backlightInterrupt); */
    /* digitalWrite(backlight, HIGH); */
    if (alarmEnabled){
      alarmEnabled = false;
    }
    else {
      alarmEnabled = true;
      ringOn = toggleValue(ringOn, "Ring Enabled:");
    }
    homeScreen();
    while (!digitalRead(enterButton)) {
      delay(100);
    }
  }
  
  if (!digitalRead(downButton)){
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    while (!digitalRead(downButton)) {
      delay(100);
    }
    setTime(&h, &m, &s, "Time:");
    homeScreen();
  }
  if (!digitalRead(upButton)){
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    while (!digitalRead(upButton)) {
      delay(100);
    }
    setTime(&alarmH, &alarmM, &alarmS, "Alarm:");
    adjustAlarm();
    homeScreen();
  }
  
  /* check for alarm */
  if (alarmFlag) {
    Int.detach(backlightInterrupt);
    digitalWrite(backlight, HIGH);
    displayClock = false;
    lcd.clear();
    lcd.home();
    lcd.print("Ring! Ring Ring!");
    displayClock = true;
    if (!fadeOn(fadeOnMinutes)){
      ring();
    }
    digitalWrite(lamp, LOW);
    alarmFlag = false;
    displayClock = false;
    lcd.begin(16, 2);
    lcd.clear();
    lcd.home();
    lcd.setCursor(0,0);
    printAlarmState();
    displayClock = true;
    backlightInterrupt = OnTimer1(backlightOff, INT_SEC, 30);
  }
  delay(100);
  }
  else {
    jumpFlag = false;
  }
}

