#define backlight 12
#define enterButton 26
#define upButton 28
#define downButton 27

#define longPress 5

/* LCD pins */
#define lcdRS 3
#define lcdEnable 2
#define lcdd0 0
#define lcdd1 0
#define lcdd2 0
#define lcdd3 0
#define lcdd4 4
#define lcdd5 5
#define lcdd6 6
#define lcdd7 7
/* lcd chars */

#define DOWN_ARROW 0
#define UP_ARROW 1

#define LINEOUT PWM2
