#define ANALOGWRITE
#define INTDISABLE 
#define LCDCLEAR
#define LCDCURSOR
#define LCDHOME
#define LCDNOCURSOR
#define LCDPRINT
#define LCDPRINTF
#define LCDSETCURSOR
#define LINEOUT PWM2
#define TMR0INT
#define TMR1INT
#define INT0INT
#define backlight 6
#define downButton 16
#define enterButton 17
#define lamp 12
#define longPress 5
#define upButton 15

/* LCD pins */
#define lcdRS 8
#define lcdEnable 1
#define lcdd0 0
#define lcdd1 0
#define lcdd2 0
#define lcdd3 0
#define lcdd4 2
#define lcdd5 3
#define lcdd6 4
#define lcdd7 5
/* lcd chars */

#define DOWN_ARROW 0
#define UP_ARROW 1


#define inverseLampLogic

#include "analog.c"
#include <audio.c>
#include "eeprom.c"
#include <delayms.c>
#include <digitalp.c>
#include <digitalr.c>
#include <digitalw.c>
#include <interrupt.c>
#include "lcdlib.c"
#include <macro.h>
#include <typedef.h>
