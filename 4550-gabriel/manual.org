#+Title: Sunrise Clock/Lamp Manual

This Clock/Lamp is the latest iteration of a design I have been
developing for about two years.  As such the code is very stable and
has been extensively tested (we use a lamp running the same code as an
alarm clock/lamp every day in Durham).  It is designed to do several
things:

- To be a gentle bedside light, fading on and off when used, to avoid
  shocking the eyes.
- To be a _dimmable_ lamp over the complete range of outputs of which
  the bulb is capable.
- To be a gentle sunrise alam, fading the light on slowly in the
  morning to wake one up naturally.
- To be create a gentle sunset, hopefully to make it easier to fall
  asleep, by slowly fading the light off in the evening.
- To be a conventional alarm clock, with optional beep (which fades
  on).

In order to do these things, it needs to be situated
properly---ideally on a bedside table or at any rate as close to the
bed as posible, and it needs to be set up properly for _you_.  This
means that the maximum and minimum brightness and the fade times might
need adjusting.  This is discussed in this manual.

Additionally, it needs a light bulb!  120v bulbs are not easily
available in this country, so I have not fitted one.  The lamp/clock
itself will work in 120v or 240v countries, at 50 or 60Hz, but the
bulb will need changing.  The socket will safely fit either a 120v
american bulb, or a 240v ES17 european bulb.  The bulb can be any
kind, but best dimming (and probably longest life) will be obtained
with a /proper incandescent light bulb/, the old fashioned kind with a
filament.  Second best is a halogen bulb.  Worst is an LED bulb, as
most of them don’t dim well.  CFLs will probably not work, but they
are very hard to get anyhow these days.

A wattage of about 40W is right for an incandescent bulb.  At this
wattage there is no risk of fire.

* Interface Paradigm
  
On top of the lamp is the bulb and shade.  In front of the lamp, on
the top, is the /Top Button/ or /Enter Button/.  On the front of the
lamp is the screen.  Below the scree are the /Down Button/ and /Up
Button/.

What is /displayed/ on the screen is controlled with the /Down Button/
and /Up Button/.  What the Lamp/Clock /does/ is controlled with the
/Enter Button/ (aka /Top Button/).  Thus the first thing the Clock
will ask you to do is to set the time.  To do this, use the up and
down buttons until the correct value is reached, and then press the
enter button to advance to the next digit.

All buttons can be /pressed/ or /held/.  /Holding/ the up and down
buttons generally causes their input to happen rapidly until they are
released (an exception is for setting the brightness of the lamp in
lamp mode), or to happen once and wait, where that makes more sense.
Holding the enter button generally causes the display to reset (an
exception is when setting a snooze after the alarm has elapsed).

The display is menu driven.  It boots up in the Main Menu.  Pressing
the up and down keys allows one to scroll through the menu.  Some
items on the Main Menu are Sub Menus: pressing the enter button causes
one to enter them.  Holding down the enter button returns one to the
main menu.  /Pressing/ the enter button ‘enters’ whatever is displayed
on the menu (except in clock mode, where it turns the lamp on or
off).  If left to itself in some function (e.g. ‘set time’) the clock
will time out on each entry after about 40 seconds (so about 120s to
timeout hours, then minutes, then seconds).  When on a menu entry, the
clock will time out back to the main menu in about 40 seconds.
Finally, when at the main menu, the backlight will fade off after
about 40 seconds.

* Operation

Plug the Lamp/Clock in and turn it on.  Set the time as requested.
The display will now be in Clock Mode: this is the default position to
which it will respond.  If you leave it for around forty seconds, the
backlight will turn off.  Press either the up or down button and the
display will come on again (without doing anything else).

** Clock

*** Setting the time

Go to System Setup -> Set Time and set the time.  As with all menu
entries, hold down the enter button once you have returned to the
‘System Setup’ menu, or wait forty seconds to return to clock mode.

*** My clock drifted!

The clock/lamp uses the mains to obtain a 1Hz signal.  (It also uses
the mains frequency for the dimming function, as described below.)  In
the UK and most of the world, mains electrical current reverses
direction in a roughly sinusoidal fashion 50 times a second (i.e. it’s
a 50Hz sine wave).  In the US of A, to be diffferent, it switches
direction /60/ times a second.  When the lamp boots up it times the
time between two zero-crossings of the waveform, and guesses whether
it is in a 60 or 50Hz country.  Hopefully this should work perfectly
(I’ve tested it here by injecting a 60Hz signal instead of the
mains).  If you find that the clock is running impossibly quickly, it’s
possible that it has made a mistake.  Go to System Setup -> Set Mains
Freq, and if it doesn’t show 60, set it to 60.  Note that there is
probably no reason to use any value other than 60 (or 50 for a 50Hz
country).

If, however, you see a few second’s drift a month, there’s nothing
much you can do about it.  That’s quite a good rate for a clock, and
the mains is very precise over long periods, so it shouldn’t get much
worse than that.

A clock using this method in Durham has never yet required adjusting
and has been running in several month increments (in between being
switched off for code updates).

** Alarm

The device has two alarm modes: firstly, the lamp fades on for a
defined period; secondly, if enabled, it beeps like a standard alarm
clock, to make sure you get up.

Every time the clock counter is advanced the clock checks whether the
time is equal to the alarm time - the fade time (obviously, this value
is precomputed).  /Thus if you try to set an alarm for less than Fade
Time minutes in the future it won’t trigger until tomorrow./  If you
want to set a very short alarm, you want the ‘nap’ feature instead
(see below).

*** Set Alarm

Press the Up button from the main screen, or go to Setup Alarm -> Set
Alarm Time.  Alarms are preserved over power cycles.

*** Enable/Disable Alarm

Go to Setup Alarm -> Enable/Disable.

*BUG*: In versions of the code prior to v1.5.1  (including the version
shipped with the lamp) I forgot actually to implement turning the
alarm off.  This has been fixed, but since the code is shipped it will
need to be updated remotely.  For this see [[Updating Firmware]].

*** Enable/Disable Ring

Set the Alarm Time.  The current alarm will be presented by default.
After the alarm time is set, the clock will ask if you want ring
enabled or not.  (Default is yes.)

*** Set Fade On Time

Go to System Setup -> Set Fade Time

Longer fade times are more like ‘natural’ sunrise.  Apparently about
30 minutes is a good starting point.

*** Turn off a sounding or fading alarm

/Press/ the top button.

*** Snooze an Alarm

/Hold/ the top button.  Snoozes between 3 minutes and 59 minutes can
be set.  The Fade On Time will be set appropriately.  When a snooze is
set, the main display shows the time remaining until the snooze
elapses.  /Pressing/ the top button when a snooze is enabled allows
you to edit the snooze.  When a snooze is set it asks you to confirm:
this can be used to cancel a snooze, by using the up or down arrows to
select ‘Cancel Snooze’.  Thus to cancel a pending snooze, /press/ the
top button edit the snooze, press the top button to accept the currnet
time, and then select ‘Cancel Snooze’.  The clock will revert to
standard clock mode.

When snoozing is over or cancelled the alarm is reset to function
properly tomorrow, exactly as with a normal alarm clock.


** Sunset

As well as acting as a sunrise, the lamp can act as a sunset to help
you fall asleep.  Press the down button from the main screen to
navigate to ‘start sunset’.  Set the sunset time and press enter.

Sunset can be cancelled by pressing the top button.

** Lamp

The device, obviously, features a lamp.  This lamp is used for the
Sunrise/sunset functions, but can also be used independently.

*** Turning the Lamp On/Off

From the main screen (to which the device defaults when there is no
activity) /hold/ the top buton.  The lamp will fade on until you let
go of the button, or until it reaches maximum brightness.  In this way
it will not jar the eyes at night.  To turn the lamp off /hold/ the
top button, and it will fade all the way off (even if you release the
button first).

*** Adjusting the Lamp Brightness

/Hold/ the down or up button from the main screen.  Set the
brightness, then press the top/enter button to return to the main screen.

*** Max/Min Brightness

The maximum available brightness is the same as a standard light
fitting.  Dimming is implemented by trailing-edge cutting:

[[./dimmer.jpg]]


(taken from http://www.lamps-on-line.com/media/wysiwyg/blog/trailing-edge-dimming.jpg)

The minimum brightness the bulb can respond to will depend on the
bulb.  Therefore when the bulb is changed, the minimum brightness
should be set by going to System Setup -> Set Min Brightness.

The Maximum brightness is the brightness at which no perceivable
increase occurs by increasing the brightness setting any more.  This
is because in 50Hz countries there are more available brightness steps
than in 60Hz countries, since the dimmer divides the waveform into
steps of a fixed width (fixed time-span), and at 60Hz one cycle takes
less time than at 50Hz.  Setting the maximum above this value will not
cause any problems, as the dimmer will in fact reset its counter every
half-cycle.  But it is wasteful of dimmer steps, as when fading the
lamp will endeavour to increase a brightness which is already at
maximum, and will thus fade on in a shorter time than desired.

Additionally, the maximum brightness should be the maximum desired
brightnes /for the room in question/, which may be less than the
maximum available brightness from the bulb.

I have tried to set a good default, but the maximum brightness can be
increased or decreased by going to System Setup -> Set Max Brightness.

** Backlight

The backlight brightness can be adjusted, to save your eyes at night.
Go to System Setup -> Set Bkl Bright.  The default is about 75%.
Brightness is an integer between 0 and 1023, although 0 is just ‘off’
and is probably useless.

** Reset

Go to Setup Menu -> Factory Reset.

* Oddities
Due to the way the clock is implemented (i.e. on a small
microcontroller, by an amateur) there are a few oddities.  A faster
microcontroller with a proper Real Time Operating System, or possibly
just better code written from scratch by a better programmer would
probably avoid these.  Suggestions for mitigation are welcome!

- Clock flicker on turning lamp On/Off :: The lamp is turned on/off
     using an interrupt routine.  Enabling the interrupt takes a few
     cycles as does servicing the interrupt during the fade.
     Furthermore, the clock screen is just another menu entry, and the
     whole menu loop is called (very quickly) when the button is
     pressed.  I have not noticed this effect much since tuning
     timings.  Rest assured that it has nothing to do with the
     clock itself, which is incremented in the background (using
     another interrupt routine), but only the display.

- Screen displaying garbage :: This can happen if electrical noise is
     picked up on the lcd terminals (unlikely), if the microprocessor
     misbehaves (for instance due to excess heat), or if noise is
     coupled through the power supply (most likely).  It should not
     happen, but if it does happen, turn the lamp off and on again.
     (Note: previous iterations of this code corrupted the screen due
     to poor interrupt code.  This has been resolved.)

- Short fades not precisely timed :: Fades on/off of over
     (maxBrightness - minBrightness) seconds use the system clock.
     Fades under this number (generally <~700s / <~12 mins) use an
     interrupt timer of tickPeriod/100.  This is integer division and
     there will likely be some remainder; as a consequence the actual
     total fade time will not be precisely the number of seconds the
     system clock has counted.  Unless you time it you should not
     notice the difference.  Short fades are only used by default when
     snoozing.

* Safety

There are no particular safety hazards with this device /if used
properly/.  However, there are some issues you should be aware of:

** Single-Wire Switching

The lamp socket should be treated as always live when changing the
bulb, even if the lamp is not on.  This is not dangerous, but should
be followed with /all/ light sockets.  Do not stick fingers in them!

** Insulation

The lamp has been constructed as double-insulated, and should thus be
safe under all conditions.  The internal PSU is fused (internally and
irreplacably, but the whole module can be replaced).

** Fire

The shade on this lamp is made from paper and has not been fire
tested, unlike commercial lampshades.  It has been run for about six
hours at full brightness.  Although the lamp fitting became warm, the
lampshade was not detectably so.  The shade has also been heated
extensively when fitting it, with no combustion.

* Maintenance

** Cleaning

Just wipe with a damp cloth.

** Replacing Bulb

The bulb can be any kind, but best dimming (and probably longest life)
will be obtained with a /proper incandescent light bulb/, the old
fashioned kind with a filament.  Second best is a halogen bulb.  Worst
is an LED bulb, as most of them don’t dim well.  CFLs will probably
not work, but they are very hard to get anyhow these days.

A wattage of about 40W is right for an incandescent bulb.  At this
wattage there is no risk of fire.


** Reprogramming

The lamp/clock is capable of being easily reprogrammed in the field
via the USB B socket on the back the base. 

When you have obtained the new firmware, use a pin to press the button
behind the small hole beside the usb socket.  The display will turn
off, and the device will be ready for new firmware.  Now run the
uploader script from the computer.  The lamp will start running
immediately the firmware is uploaded.

See [[Updating Firmware]].

** Source Code

The source code for this lamp (together with this manual) is available
at

https://gitlab.com/2e0byo/sunrise_clock/-/tree/2.0/4550-gabriel

It was initially written using the [[http:pinguino.cc][Pinguino]] ide, and then the
generated C code was rather crudely copied into a repository and
edited by hand.  Currently it will not compile without the [[https://github.com/PinguinoIDE/pinguino-libraries][Pinguino
Libraries]], although several of these have been modified.  The compiled
code is designed to be uploaded to a PIC18f device flashed with the
pinguino bootloader, although it will probably work if programmed
directly with a programmer.

* Feedback

A lot of thought has gone into the interface design---hopefully the
most used functions are most easily accessible, and things increment
at sensible rates.  Things roll over when it seems sensible, and cap
at max/min when it doesn’t.  Feedback on all these decisions and
others is welcome.

* Technical

The lamp has two sides: the mains-facing side, and the controller.  On
the mains facing side is a mosfet switch controlling the lamp (think
of it as an electrically controllable silent switch capable of turning
on and off thousands of times a second).  There is also a
zero-crossing detector, produces a 5v square-wave at the same
frequency as the mains, without any electrical connection accross it
(it uses light, in an optocoupler).  There is also a 5v power supply.
More of that in the blog post.

On the other side (separated by a partiition for insulation) is a
small computer, a Pic 18f4550, which recieves the 5v mains-frequency,
and the buttons, and controls the speaker (via a low-pass filter, so
we can feed it a square wave and get a nice pure buzz) and the lcd.

The whole thing can be taken apart by undoing the screws at the
bottom, and then _carefully_ sliding the base out.

* Updating Firmware

Field updates of firmware (the software running on the clock’s MCU)
are possible over USB.  A USB-B to -A lead is needed.  Until recently
these were shipped with all printers, and you probably have one in a
box of old printer cables (everyone seems to have such a box).  If not
they can generally be picked up inexpensively.

To update the firmware, plug the cable in the lamp (hole at the back
to access the USB socket---be gentle with it as it is not too firmly
fixed inside), plug the other end into a computer.  Then:

1. Open the update software.
2. Using an unbent paperclip, press the small switch inside the lamp
   beside the USB socket.  This switch resembles a small rectangle,
   about 1/16” high and 1/32” wide.  It is directly to the right
   (possibly left?!) of the usb socket.  When it is pressed, the lamp
   display will turn off.
3. The lamp is now in firmware update mode.  Press the ‘upload
   firmware’ button in the software.  When firmware is successfully
   uploaded the lamp display will come on and it will start operating
   as normal.
4. /Carefully/ unplug the USB cable from the lamp.

** Where do I get the firmware uploading software?

https://github.com/Mad-Wombat-Labs/pinguino-firmware-uploader/releases

Download the ~Pinguino-Firmware-Installer{something}.exe~ file and
then run it.  Detailed instructions are on the wiki

Erm.  Hmm.  I need to write it.  If you run Linux I can throw a script
together in ten minutes. (I already have one, it just needs a
graphical interface).  But since I think you’re on Windows, I need to
write some windows code.  Will do it in a few days.

* Bugs

** v1.5.0
- [ ] Bug prevent disabling alarms from functioning at all.
