/* HAL since multiple methods of controlling the lamp are used */

#define lamp LATAbits.LATA0
#define maxduty 600
#define minduty 0
#define TRAILING_EDGE_DIMMER 1

u8 dimmerInterrupt = 0;
unsigned short duty = 0;
unsigned short cycleCounter = 0;

/* used to shape the waveform */
void lampISR(){
  if (++cycleCounter >= duty) {
    lamp = 0;
    IntDisable(dimmerInterrupt);
  }
}

void setupTimer1(){
  /* Setup timer 1 as the lamp ticker to give resolution ticks per
     half waveform.  This is copied from Pinguino interrupt.c */
    u8  _presca_;
    unsigned short _cycles_;
    u32 fosc = System_getPeripheralFrequency();

    if (intUsed[INT_TMR1] == INT_NOT_USED)
    {
        intUsed[INT_TMR1] = INT_USED;
        intCount[INT_TMR1] = 0;
        intFunction[INT_TMR1] = lampISR;

	_presca_ = T1_PS_1_4;
	_cycles_ = 0xFFFF - 45;
        preloadH[INT_TMR1] = high8(_cycles_);
        preloadL[INT_TMR1] = low8(_cycles_);

        TMR1H = preloadH[INT_TMR1];
        TMR1L = preloadL[INT_TMR1];
        T1CON = T1_ON | T1_16BIT | T1_SYNC_EXT_ON | _presca_ | T1_SOURCE_FOSCDIV4;
        IPR1bits.TMR1IP = INT_LOW_PRIORITY;
        PIR1bits.TMR1IF = 0;
        PIE1bits.TMR1IE = INT_ENABLE;
    }
    dimmerInterrupt = INT_TMR1;
}


void setLampBrightness(unsigned short *lampBrightness) {
  duty = *lampBrightness;
}

void tick(){
  if (!duty) return;
  lamp = 1;
  cycleCounter = 0;
  IntEnable(dimmerInterrupt);
}

void ticktock(){
  second();
  if (!duty) return;
  lamp = 1;
  cycleCounter = 0;
  IntEnable(dimmerInterrupt);
}


void initLamp(){
  TRISAbits.TRISA0 = 0;		/* lamp as output  */
  lamp = 1;
  setupTimer1();
  IntEnable(dimmerInterrupt);
}
