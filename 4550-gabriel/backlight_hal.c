/* HAL since multiple methods of controlling the backlight are used */
#define BACKLIGHT_BRIGHTNESS

bit backlightStatus;
unsigned short backlightBrightness;

char brightnessIsBacklight = 0;

static const u8 eepromBacklightBrightness = 11;

void backlightOn(){
  unsigned short bkl = 0;
  while (++bkl < backlightBrightness) {
    analogwrite(backlight, bkl);
    Delayus(300);
  }
  backlightStatus = 1;
}

void backlightOff(){
  if (!backlightStatus) return;	/* called repeatedly by timeout loop */
  
  unsigned short bkl = backlightBrightness;
  while (bkl-- > 0) {
    analogwrite(backlight, bkl);
    Delayus(300);
  }
  backlightStatus = 0;
}


void setBacklightBrightness(){
  brightnessIsBacklight = 1;
  setBrightness(&backlightBrightness, "Backlight on:", "Value:", 1023, 0);
  brightnessIsBacklight = 0;
  EEPROM_write16(eepromBacklightBrightness, backlightBrightness);
}

void backlightInit(){
  backlightBrightness = EEPROM_read16(eepromBacklightBrightness);
  if (backlightBrightness > 1023) backlightBrightness = 1023;
  pinmode(backlight, OUTPUT);
}
